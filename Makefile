BIN_DIR = bin/
SRC_DIR = src/
TESTS_DIR = tests/
MKDIR_BIN = mkdir -p $(BIN_DIR)

HB_FILES_DIR = hb-files/
FTP_HB_FILES_DIR = ftp://math.nist.gov/pub/MatrixMarket2/Harwell-Boeing/

CC = gcc
CFLAGS = -g -Wall -lm -lOpenCL
OBJS = $(patsubst %.c, %.o, $(wildcard $(SRC_DIR)*.c))
TESTS = $(wildcard $(TESTS_DIR)*_test.c)
MAIN_NAME = $(BIN_DIR)conjugate-gradient

install:
	$(MKDIR_BIN)
	$(MAKE) compile-main
	$(MAKE) clean-objects

test:
ifeq ($(wildcard $(HB_FILES_DIR)*),)
	$(MAKE) download-hb-files
endif
	$(MKDIR_BIN)
	$(MAKE) compile-tests
	$(MAKE) run-tests
	$(MAKE) clean-objects
	$(MAKE) clean-tests

download-hb-files:
	rm -rf $(HB_FILES_DIR)
	mkdir $(HB_FILES_DIR)
	wget -P $(HB_FILES_DIR) -r -nd -A '*.rsa.gz,*.rua.gz' \
		$(FTP_HB_FILES_DIR)bcsstruc1 \
		$(FTP_HB_FILES_DIR)bcsstruc2 \
		$(FTP_HB_FILES_DIR)bcsstruc3 \
		$(FTP_HB_FILES_DIR)chemimp \
		$(FTP_HB_FILES_DIR)chemwest \
		$(FTP_HB_FILES_DIR)cirphys \
		$(FTP_HB_FILES_DIR)counterx \
		$(FTP_HB_FILES_DIR)dwt \
		$(FTP_HB_FILES_DIR)econaus \
		$(FTP_HB_FILES_DIR)econiea \
		$(FTP_HB_FILES_DIR)facsimile \
		$(FTP_HB_FILES_DIR)gemat \
		$(FTP_HB_FILES_DIR)grenoble \
		$(FTP_HB_FILES_DIR)jagmesh \
		$(FTP_HB_FILES_DIR)lanpro \
		$(FTP_HB_FILES_DIR)laplace \
		$(FTP_HB_FILES_DIR)lapu \
		$(FTP_HB_FILES_DIR)lns \
		$(FTP_HB_FILES_DIR)lockheed \
		$(FTP_HB_FILES_DIR)lshape \
		$(FTP_HB_FILES_DIR)lsq \
		$(FTP_HB_FILES_DIR)manteuffel \
		$(FTP_HB_FILES_DIR)nnceng \
		$(FTP_HB_FILES_DIR)nucl \
		$(FTP_HB_FILES_DIR)oilgen \
		$(FTP_HB_FILES_DIR)platz \
		$(FTP_HB_FILES_DIR)pores \
		$(FTP_HB_FILES_DIR)psadmit \
		$(FTP_HB_FILES_DIR)psmigr \
		$(FTP_HB_FILES_DIR)saylor \
		$(FTP_HB_FILES_DIR)steam \
		$(FTP_HB_FILES_DIR)smtape \
		$(FTP_HB_FILES_DIR)sherman \
		$(FTP_HB_FILES_DIR)watt
	gunzip $(HB_FILES_DIR)*

compile-main: $(SRC_DIR)main.o $(OBJS)
	$(CC) -o $(MAIN_NAME) $(patsubst $(SRC_DIR)%, $(BIN_DIR)%, $?) $(CFLAGS)

compile-tests: $(filter-out $(SRC_DIR)main.o, $(OBJS))
	$(foreach TEST, $(TESTS), $(CC) -o $(BIN_DIR)$(basename $(notdir $(TEST))) $(TEST) $(patsubst $(SRC_DIR)%, $(BIN_DIR)%, $?) $(CFLAGS);)

run-tests:
	$(foreach TEST, $(TESTS), ./$(BIN_DIR)$(basename $(notdir $(TEST)));)

clean-objects:
	rm $(BIN_DIR)*.o -f

clean-tests:
	rm $(BIN_DIR)*_test -f

%.o: %.c
	$(CC) $(CFLAGS) -c -o $(BIN_DIR)$(notdir $@) $<