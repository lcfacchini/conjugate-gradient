#include "../src/clenviroment.h"
#include "../src/hbmatrix.h"
#include "../src/cgutils.h"
#include "../src/hbfreader.h"
#include "common/data.h"
#include "../src/utils.h"

#include "../src/cg.h"

int test_CLHB_matrix_product_vector(CL_Enviroment *env, HB_Matrix *matrix) {
	HB_Matrix *matrixCsr = HB_csc2csr(matrix);

	unsigned int i = 0, eq = 0, numVariables = (matrix->sizePointers - 1);
	double vector[numVariables];
	for (i = 0; i < numVariables; i++) {
		vector[i] = 1.0/ rand();
	}
	//CGU_print_vector(vector, numVariables, NULL);
    double *result = malloc(sizeof(double) * numVariables);
    double *result2 = malloc(sizeof(double) * numVariables);
    double *result3 = malloc(sizeof(double) * numVariables);

	HB_Matrix_CL *matrixCl = malloc(sizeof(HB_Matrix_CL));
	CG_alloc_matrix_cl(env, matrixCl, matrixCsr, matrix);
	cl_kernel matrixProductVector = CL_create_kernel(env, "matrix_product_vector");

	HB_matrix_product_vector(matrix, vector, result, numVariables, 0);
	CLHB_matrix_product_vector(env, matrixProductVector, matrixCsr, matrixCl, vector, result2, 0);
	eq = assert_double_vector_equals_epsilon(result, result2, numVariables, EPSILON_DOUBLE_COMPARISON);

	HB_matrix_product_vector(matrix, vector, result, numVariables, 1);
	CLHB_matrix_product_vector(env, matrixProductVector, matrixCsr, matrixCl, vector, result3, 1);
	eq |= assert_double_vector_equals_epsilon(result, result3, numVariables, EPSILON_DOUBLE_COMPARISON);

	CG_release_alloc_matrix_cl(matrixCl);

	return eq;
}

int test_CLHB_vector_product_vector(CL_Enviroment *env, unsigned int n) {
	double *vector1 = generate_sequential_vector(n);
    double *vector2 = generate_sequential_vector(n);

	double resultSeq = CG_vector_product_vector(vector1, vector2, n);

	cl_kernel vectorProductVector = CL_create_kernel(env, "vector_product_vector");
	double resultCl = CLCG_vector_product_vector(env, vectorProductVector, vector1, vector2, n);
	return assert_double_equals(resultSeq, resultCl);
}

int test_CLHB_vector_op_vector(CL_Enviroment *env, unsigned int n) {
	unsigned int eq;
	double *vector1 = generate_sequential_vector(n);
    double *vector2 = generate_sequential_vector(n);
	double *result = malloc(sizeof(double) * n);
	double *resultCl = malloc(sizeof(double) * n);
	double scalar = 32;
	cl_kernel kernelSum = CL_create_kernel(env, "vector_sum_vector");
	cl_kernel kernelSub = CL_create_kernel(env, "vector_subtraction_vector");
	cl_kernel kernelSumScalarVector = CL_create_kernel(env, "vector_sum_scalar_product_vector");
	cl_kernel kernelSubScalarVector = CL_create_kernel(env, "vector_sub_scalar_product_vector");

	CG_vector_sum_vector(vector1, vector2, result, n);
	CLCG_vector_op_vector(env, kernelSum, vector1, vector2, resultCl, n);
	eq = assert_double_vector_equals(result, resultCl, n);

	CG_vector_subtraction_vector(vector1, vector2, result, n);
	CLCG_vector_op_vector(env, kernelSub, vector1, vector2, resultCl, n);
	eq &= assert_double_vector_equals(result, resultCl, n);

	cl_mem clScalar = clCreateBuffer(env->context, CL_MEM_READ_ONLY, sizeof(double), NULL, NULL);
	int err = clEnqueueWriteBuffer(env->queue, clScalar, CL_TRUE, 0, sizeof(double), &scalar, 0, NULL, NULL);
    err |= clSetKernelArg(kernelSumScalarVector, 3, sizeof(cl_mem), &clScalar);
    err |= clSetKernelArg(kernelSubScalarVector, 3, sizeof(cl_mem), &clScalar);
	if (err != CL_SUCCESS) {
        printf("Error: failed to set args!\n");
        exit(1);
    }

	CG_vector_op_scalar_product_vector(vector1, '+', scalar, vector2, result, n);
	CLCG_vector_op_vector(env, kernelSumScalarVector, vector1, vector2, resultCl, n);
	eq &= assert_double_vector_equals(result, resultCl, n);

	CG_vector_op_scalar_product_vector(vector1, '-', scalar, vector2, result, n);
	CLCG_vector_op_vector(env, kernelSubScalarVector, vector1, vector2, resultCl, n);
	eq &= assert_double_vector_equals(result, resultCl, n);

	return eq;
}

int main(int const argc, char *argv[]) {
	printf("%-15s ", basename(argv[0]));

	CL_Enviroment *env = CL_start_env();
	if (env == NULL) {
		printf("\nErro\n");
		return 0;
	}

	int result = 1;


	HB_Matrix matrix1 = TEST_MATRIX1_SY;
	HB_Matrix matrix3 = TEST_MATRIX3_UN;
	HB_Matrix *bcsstk16 = HBFR_read("hb-files/bcsstk16.rsa");
	HB_Matrix *impcola = HBFR_read("hb-files/orsirr_1.rua");


	result = /* test_CLHB_vector_op_vector(env, 105) |
				test_CLHB_vector_op_vector(env, 199) |
				test_CLHB_vector_op_vector(env, 1000) |
				test_CLHB_vector_op_vector(env, 20) |
				test_CLHB_vector_op_vector(env, 3101) |
				test_CLHB_vector_op_vector(env, 9999) | */
				test_CLHB_matrix_product_vector(env, &matrix1) |
				test_CLHB_matrix_product_vector(env, &matrix3) |
				test_CLHB_matrix_product_vector(env, bcsstk16)  |
				test_CLHB_matrix_product_vector(env, impcola)  /* |
			 	test_CLHB_vector_product_vector(env, 48) |
				test_CLHB_vector_product_vector(env, 66) |
				test_CLHB_vector_product_vector(env, 112) |
				test_CLHB_vector_product_vector(env, 132) |
				test_CLHB_vector_product_vector(env, 256) |
				test_CLHB_vector_product_vector(env, 420) |
				test_CLHB_vector_product_vector(env, 1074) |
				test_CLHB_vector_product_vector(env, 2000) |
				test_CLHB_vector_product_vector(env, 2003) |
				test_CLHB_vector_product_vector(env, 10000)  */;

	return result ? EXIT_SUCCESS : EXIT_FAILURE;
}