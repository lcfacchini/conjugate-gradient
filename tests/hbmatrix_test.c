#include <stdlib.h>
#include <stdio.h>
#include <libgen.h>
#include "../src/hbmatrix.h"
#include "common/data.h"
#include "../src/utils.h"
#include "../src/hbfreader.h"

int test_HB_matrix_product_vector_unsymmetric() {
	HB_Matrix matrix = TEST_MATRIX2_UN;

	double vector[] = {3, 3};
	double resultVector[] = {0, 0};
	HB_matrix_product_vector(&matrix, vector, resultVector, 2, 0);

	double expectedValues[] = {6, 3};
	return assert_double_vector_equals(expectedValues, resultVector, 2);
}

int test_HB_matrix_product_vector_symmetric() {
	HB_Matrix matrix = TEST_MATRIX1_SY;

	double vector[] = {3, 4, 5};
	double resultVector[] = {0, 0, 0};
	HB_matrix_product_vector(&matrix, vector, resultVector, 3, 0);

	double expectedValues[] = {23, 25, 42};
	return assert_double_vector_equals(expectedValues, resultVector, 3);
}

int test_HB_matrix_product_vector_transpose() {
	HB_Matrix matrix = TEST_MATRIX1_UN;

	double vector[] = {1, 1, 1};
	double resultVector[] = {0, 0, 0};
	HB_matrix_product_vector(&matrix, vector, resultVector, 3, 1);

	double expectedValues[] = {5, 5, 2};
	return assert_double_vector_equals(expectedValues, resultVector, 3);
}

int assert_HB_csc2csr_equals(HB_Matrix matrix) {
	HB_Matrix *matrixB = HB_csc2csr(&matrix);

	unsigned int numVariables = matrix.sizePointers - 1;

	double *denseMatrix = HB_build_dense_matrix(&matrix);
	double *denseMatrixB = HB_build_dense_matrix(matrixB);

	int assert = assert_double_vector_equals(denseMatrix, denseMatrixB, numVariables * numVariables);

    return assert;
}

int main(int argc, char *argv[]) {
	printf("%-15s ", basename(argv[0]));

	HB_Matrix matrix1 = TEST_MATRIX1_SY;
	HB_Matrix matrix3 = TEST_MATRIX3_SY;
	HB_Matrix matrix3u = TEST_MATRIX3_UN;
    HB_Matrix *bcsstk01 = HBFR_read("hb-files/bcsstk01.rsa");
    HB_Matrix *bcsstk14 = HBFR_read("hb-files/bcsstk14.rsa");
    HB_Matrix *bcsstk15 = HBFR_read("hb-files/bcsstk15.rsa");
    HB_Matrix *bcsstk16 = HBFR_read("hb-files/bcsstk16.rsa");
    HB_Matrix *bcsstk17 = HBFR_read("hb-files/bcsstk17.rsa");
    HB_Matrix *bcsstk18 = HBFR_read("hb-files/bcsstk18.rsa");
	HB_Matrix *impcola = HBFR_read("hb-files/impcol_a.rua");
	HB_Matrix *impcold = HBFR_read("hb-files/impcol_d.rua");
	HB_Matrix *impcole = HBFR_read("hb-files/impcol_e.rua");
	HB_Matrix *orsirr1 = HBFR_read("hb-files/orsirr_1.rua");
	HB_Matrix *orsirr2 = HBFR_read("hb-files/orsirr_2.rua");

	int result = test_HB_matrix_product_vector_symmetric() &
                test_HB_matrix_product_vector_unsymmetric() &
                test_HB_matrix_product_vector_transpose() &
                assert_HB_csc2csr_equals(matrix1) &
				assert_HB_csc2csr_equals(matrix3) &
				assert_HB_csc2csr_equals(matrix3u) &
                assert_HB_csc2csr_equals(*bcsstk01) &
                assert_HB_csc2csr_equals(*bcsstk14) &
                assert_HB_csc2csr_equals(*bcsstk15) &
                assert_HB_csc2csr_equals(*bcsstk16) &
                assert_HB_csc2csr_equals(*bcsstk17) &
                assert_HB_csc2csr_equals(*bcsstk18) &
                assert_HB_csc2csr_equals(*impcola) &
                assert_HB_csc2csr_equals(*impcold) &
                assert_HB_csc2csr_equals(*impcole) &
                assert_HB_csc2csr_equals(*orsirr1) &
                assert_HB_csc2csr_equals(*orsirr2);

	printf("\n");

	return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
