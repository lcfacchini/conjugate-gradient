#include <stdio.h>
#include <libgen.h>
#include "common/utils.h"
#include "common/data.h"

#include "../src/cg.h"

int assert_CG_conjugate_gradient(enum CG_Method method,
								 HB_Matrix *matrix,
								 double *vectorB,
								 double *expectedValues,
								 PC_Matrix *precondMatrix)
{
	CG_Result *result = (CG_Result *) malloc(sizeof(CG_Result));
	result->matrix = matrix;
	result->numIterations = 1;
	result->variables = (double *) calloc(matrix->sizePointers - 1, sizeof(double));

	switch (method) {
		case CG:
			CG_conjugate_gradient(matrix, vectorB, result, CG_MAX_ITERATIONS, CG_EPSILON, precondMatrix);

			break;
		case BICG:
			CG_biconjugate_gradient(matrix, vectorB, result, CG_MAX_ITERATIONS, CG_EPSILON, precondMatrix);

			break;
	}

	int equal, match = 1;
	unsigned int i;
	for (i = 0; i < (result->matrix->sizePointers - 1); i++) {
		equal = expectedValues[i] == result->variables[i];
		match &= equal;

		printf("%c", equal ? '.' : 'F');
	}

	return match;
}

int test_CG_conjugate_gradient1() {
	HB_Matrix matrix = TEST_MATRIX2_SY;
	double vectorResults[] = {2, 2};
	double expectedValues[] = {1, 1};

	return assert_CG_conjugate_gradient(CG, &matrix, vectorResults, expectedValues, NULL);
}

int test_CG_conjugate_gradient2() {
	unsigned int pointers[] = {1, 2, 3, 4, 5};
	unsigned int indices[] = {1, 2, 3, 4};
	double values[] = {1, 1, 1, 1};
	HB_Matrix matrix = {".", 5, 4, 4, pointers, indices, values, HB_MATRIX_TYPE_SYMMETRIC};
	double vectorResults[] = {1, 2, 3, 4};
	double expectedValues[] = {1, 2, 3, 4};

	return assert_CG_conjugate_gradient(CG, &matrix, vectorResults, expectedValues, PC_polynomial(&matrix));
}

int test_CG_conjugate_gradient3() {
	unsigned int pointers[] = {1, 2, 4};
	unsigned int indices[] = {1, 1, 2};
	double values[] = {3, 2, 3};
	HB_Matrix matrix = {".", 3, 3, 3, pointers, indices, values, HB_MATRIX_TYPE_SYMMETRIC};
	double vectorResults[] = {2, -2};
	double expectedValues[] = {2, -2};

	return assert_CG_conjugate_gradient(CG, &matrix, vectorResults, expectedValues, NULL);
}

int main(int const argc, char *argv[]) {
	printf("%-15s ", basename(argv[0]));

	int result = test_CG_conjugate_gradient1() &
	 	test_CG_conjugate_gradient2() &
	 	test_CG_conjugate_gradient3();

	printf("\n");

	return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
