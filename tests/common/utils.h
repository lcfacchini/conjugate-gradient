#ifndef UTILS_H
#define UTILS_H

#include "../../src/hbmatrix.h"
#include "../../src/precond.h"

// 0.000001
#define EPSILON_DOUBLE_COMPARISON 0.000001
#define EPSILON_DOUBLE_MIN_COMPARISON 0.001

int assert_double_vector_equals_epsilon(double *expected, double *actual, int size, double epsilon);

int assert_double_vector_equals(double *expected, double *actual, int size);

int assert_double_equals_epsilon(double expected, double actual, double epsilon);

int assert_double_equals(double expected, double actual);

double *generate_sequential_vector(unsigned int size);

#endif