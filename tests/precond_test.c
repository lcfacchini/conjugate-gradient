#include <stdio.h>
#include <libgen.h>
#include "../src/hbmatrix.h"
#include "../src/precond.h"
#include "../src/hbfreader.h"
//#include "../src/cgutils.h"
#include "../src/utils.h"
#include "common/data.h"

int test_PC_diagonal() {
	HB_Matrix matrix = TEST_MATRIX1_UN;
    PC_Matrix* precondMatrix = PC_diagonal(&matrix);

    double expectedDiagonal[] = {1, 0, 0.5};
    return assert_double_vector_equals(expectedDiagonal, precondMatrix->daMatrix->values, 3);
}

int test_PC_polynomial_unsymmetric() {
	HB_Matrix matrix = TEST_MATRIX3_UN;
    PC_Matrix *precondMatrix = PC_polynomial(&matrix);

	double expected[] = {0.200000, 0.257143, 0.233333, 0.257143, 0.257143,
	0.142857, 0.095238, 0.163265, 0.033333, 0.071429, 0.166667, 0.071429,
	0.057143, 0.122449, 0.142857, 0.142857};

    return assert_double_vector_equals(expected, precondMatrix->hbMatrix->values, precondMatrix->hbMatrix->sizeValues - 1);
}

int test_PC_polynomial_symmetric() {
	HB_Matrix matrix = TEST_MATRIX3_SY;
	PC_Matrix *precondMatrix = PC_polynomial(&matrix);

	double expected[] = {0.200000, 0.257143, 0.233333, 0.257143, 0.142857, 0.095238, 0.163265, 0.166667, 0.071429, 0.142857};

	return assert_double_vector_equals(expected, precondMatrix->hbMatrix->values, precondMatrix->hbMatrix->sizeValues - 1);
}

int main(int const argc, char *argv[]) {
	printf("%-15s ", basename(argv[0]));

	int result = test_PC_diagonal() & test_PC_polynomial_unsymmetric() & test_PC_polynomial_symmetric();

	printf("\n");

	return result ? EXIT_SUCCESS : EXIT_FAILURE;
}