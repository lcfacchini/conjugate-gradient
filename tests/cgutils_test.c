#include <stdio.h>
#include <libgen.h>
#include "../src/cgutils.h"

int assert_equal_unsigned_long(unsigned long expected, unsigned long atual) {
	return expected == atual;
}

int assert_CGU_formated_time(double time, unsigned long expectedHour, unsigned long expectedMinute, unsigned long expectedSecond, unsigned long expectedMs) {
	CGU_Time* gcutime = CGU_formated_time(time);

	int result = assert_equal_unsigned_long(gcutime->hour, expectedHour) &
	assert_equal_unsigned_long(gcutime->minute, expectedMinute) &
	assert_equal_unsigned_long(gcutime->second, expectedSecond);
	assert_equal_unsigned_long(gcutime->ms, expectedMs);
	CGU_time_free(gcutime);

	printf("%c", result ? '.' : 'F');

	return result;
}

int test_CGU_formated_time_hour() {
	return assert_CGU_formated_time(3600, 1, 0, 0, 0) &
		assert_CGU_formated_time(7200, 2, 0, 0, 0) &
		assert_CGU_formated_time(18000, 5, 0, 0, 0) &
		assert_CGU_formated_time(36000, 10, 0, 0, 0);
}

int test_CGU_formated_time_minute() {
	return assert_CGU_formated_time(60, 0, 1, 0, 0) &
		assert_CGU_formated_time(120, 0, 2, 0, 0) &
		assert_CGU_formated_time(420, 0, 7, 0, 0) &
		assert_CGU_formated_time(600, 0, 10, 0, 0);
}

int test_CGU_formated_time_secund() {
	return
		assert_CGU_formated_time(6, 0, 0, 6, 0) &
		assert_CGU_formated_time(10, 0, 0, 10, 0) &
		assert_CGU_formated_time(23, 0, 0, 23, 0) &
		assert_CGU_formated_time(31, 0, 0, 31, 0) &
		assert_CGU_formated_time(42, 0, 0, 42, 0) &
		assert_CGU_formated_time(58, 0, 0, 58, 0);
}

int test_CGU_formated_time_ms() {
	return assert_CGU_formated_time(0.9, 0, 0, 0, 900) &
		assert_CGU_formated_time(0.08, 0, 0, 0, 80) &
		assert_CGU_formated_time(0.007, 0, 0, 0, 7) &
		assert_CGU_formated_time(0.654, 0, 0, 0, 654) &
		assert_CGU_formated_time(0.123, 0, 0, 0, 123) &
		assert_CGU_formated_time(0.6666666, 0, 0, 0, 666) &
		assert_CGU_formated_time(0.0033333, 0, 0, 0, 3);
}

int test_CGU_formated_time_all() {
	return assert_CGU_formated_time(13495.007, 3, 44, 55, 7) &
		assert_CGU_formated_time(86893.100, 24, 8, 13, 100) &
		assert_CGU_formated_time(18666.1999, 5, 11, 6, 199) &
		assert_CGU_formated_time(14222.1999, 3, 57, 2, 199) &
		assert_CGU_formated_time(0, 0, 0, 0, 0);
}

int main(int argc, char *argv[]) {
	printf("%-15s ", basename(argv[0]));

	int result = test_CGU_formated_time_hour() &
		test_CGU_formated_time_minute() &
		test_CGU_formated_time_secund() &
		test_CGU_formated_time_ms() &
		test_CGU_formated_time_all();

	printf("\n");

	return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
