#ifndef CGCL_H
#define CGCL_H

#include "cg.h"


void CG_conjugate_gradient_cl(HB_Matrix *matrix,
							HB_Matrix_CL *matrixCl,
							double *vectorB,
							CG_Result *result,
							unsigned int maxIterations,
							double epsilon,
							PC_Matrix *precondMatrix,
							CL_Enviroment *env);

void CG_biconjugate_gradient_cl(HB_Matrix *matrix,
								HB_Matrix_CL *matrixCl,
								double *vectorB,
								CG_Result *result,
								unsigned int maxIterations,
								double epsilon,
								PC_Matrix *precondMatrix,
								CL_Enviroment *env);


HB_Matrix_CL *CG_alloc_matrix_cl(CL_Enviroment *clEnv, HB_Matrix_CL *matrixCl, HB_Matrix *matrixCsr, HB_Matrix *matrixCsc);

void CG_release_alloc_matrix_cl(HB_Matrix_CL *matrixCl);

void CG_alloc_precond_matrix_cl(CL_Enviroment *env, PC_Matrix *precondMatrix, cl_mem *clDiagMatrixPrecond, HB_Matrix_CL *precondMatrixCl);

void CG_precond_matrix_product_vector_cl(CL_Enviroment *env,
                                        PC_Matrix *matrix,
                                        HB_Matrix_CL *precondMatrixCl,
                                        cl_mem *clDiagMatrixPrecond,
                                        cl_mem *clVector,
                                        cl_mem *clResult,
                                        unsigned int size);




#endif

