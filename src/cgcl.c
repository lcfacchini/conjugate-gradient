#include "cgcl.h"

void CG_biconjugate_gradient_cl(HB_Matrix *matrix,
								HB_Matrix_CL *matrixCl,
								double *vectorB,
								CG_Result *result,
								unsigned int maxIterations,
								double epsilon,
								PC_Matrix *precondMatrix,
								CL_Enviroment *env)
{
	unsigned int numVariables = matrix->sizePointers - 1, precond = precondMatrix != NULL, convergence = 0;

	cl_mem clDiagMatrixPrecond;
	HB_Matrix_CL *precondMatrixCl = malloc(sizeof(HB_Matrix_CL));
	if (precond) {
		CG_alloc_precond_matrix_cl(env, precondMatrix, &clDiagMatrixPrecond, precondMatrixCl);
	}

	size_t global[] = {numVariables, 1};

	double rho, rho0, res;

	cl_mem clVariables = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorAux = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorB = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorR = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorR2 = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorP = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorP2 = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorV = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorV2 = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);

	cl_mem clAlpha = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clBeta = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clRho = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clRho0 = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clRes = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clSize = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(unsigned int), NULL, NULL);
	cl_mem clConvergedIteration = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(unsigned int), NULL, NULL);
	cl_mem clEpsilon = clCreateBuffer(env->context,  CL_MEM_READ_ONLY, sizeof(double), NULL, NULL);
	cl_mem clConvergence = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(unsigned int), NULL, NULL);
	cl_mem clPrecond = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(unsigned int), NULL, NULL);

	clEnqueueWriteBuffer(env->queue, clVectorB, CL_TRUE, 0, sizeof(double) * numVariables, vectorB, 0, NULL, NULL);
	clEnqueueWriteBuffer(env->queue, clSize, CL_TRUE, 0, sizeof(unsigned int), &numVariables, 0, NULL, NULL);

	size_t workGroupSizeVpv[] = {numVariables <= MAX_WORK_GROUP_SIZE ? MIN_WORK_GROUP_SIZE : MAX_WORK_GROUP_SIZE, 1};
    size_t globalVpv[] = {((numVariables / workGroupSizeVpv[0]) * workGroupSizeVpv[0]) + workGroupSizeVpv[0], 1};

	cl_mem clGroupResult = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(double) * (numVariables / workGroupSizeVpv[0]), NULL, NULL);
	cl_mem clPartialRes = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(double) * (numVariables / workGroupSizeVpv[0]), NULL, NULL);
	cl_mem clPartialBeta = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(double) * (numVariables / workGroupSizeVpv[0]), NULL, NULL);
	cl_mem clPartialAlpha = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(double) * (numVariables / workGroupSizeVpv[0]), NULL, NULL);

	unsigned int groupQty = globalVpv[0] / workGroupSizeVpv[0];
	cl_mem clGroupQty = clCreateBuffer(env->context,  CL_MEM_READ_ONLY, sizeof(unsigned int), NULL, NULL);
	clEnqueueWriteBuffer(env->queue, clGroupQty, CL_TRUE, 0, sizeof(unsigned int), &groupQty, 0, NULL, NULL);
	size_t globalTotal[] = {1, 1};

	cl_kernel zeroVectors = CL_create_kernel(env, "zero_vector");
    clSetKernelArg(zeroVectors, 0, sizeof(cl_mem), &clVariables);
	clEnqueueNDRangeKernel(env->queue, zeroVectors, 1, NULL, global, NULL, 0, NULL, NULL);

	clSetKernelArg(zeroVectors, 0, sizeof(cl_mem), &clVectorP);
	clEnqueueNDRangeKernel(env->queue, zeroVectors, 1, NULL, global, NULL, 0, NULL, NULL);

	clSetKernelArg(zeroVectors, 0, sizeof(cl_mem), &clVectorP2);
	clEnqueueNDRangeKernel(env->queue, zeroVectors, 1, NULL, global, NULL, 0, NULL, NULL);

	clEnqueueWriteBuffer(env->queue, clConvergedIteration, CL_TRUE, 0, sizeof(unsigned int), &(result->numIterations), 0, NULL, NULL);
	clEnqueueWriteBuffer(env->queue, clEpsilon, CL_TRUE, 0, sizeof(double), &epsilon, 0, NULL, NULL);
	clEnqueueWriteBuffer(env->queue, clConvergence, CL_TRUE, 0, sizeof(unsigned int), &convergence, 0, NULL, NULL);
	clEnqueueWriteBuffer(env->queue, clPrecond, CL_TRUE, 0, sizeof(unsigned int), &precond, 0, NULL, NULL);

	unsigned int transpose = 0;
    cl_mem clTransposeFalse = clCreateBuffer(env->context, CL_MEM_READ_ONLY, sizeof(unsigned int), NULL, NULL);
    clEnqueueWriteBuffer(env->queue, clTransposeFalse, CL_TRUE, 0, sizeof(unsigned int), &transpose, 0, NULL, NULL);

	transpose = 1;
    cl_mem clTransposeTrue = clCreateBuffer(env->context, CL_MEM_READ_ONLY, sizeof(unsigned int), NULL, NULL);
    clEnqueueWriteBuffer(env->queue, clTransposeTrue, CL_TRUE, 0, sizeof(unsigned int), &transpose, 0, NULL, NULL);

	// r = b - A * x
	clSetKernelArg(env->kernels->matrixProductVector, 0, sizeof(cl_mem), &(matrixCl->pointersCsr));
    clSetKernelArg(env->kernels->matrixProductVector, 1, sizeof(cl_mem), &(matrixCl->indicesCsr));
    clSetKernelArg(env->kernels->matrixProductVector, 2, sizeof(cl_mem), &(matrixCl->valuesCsr));
    clSetKernelArg(env->kernels->matrixProductVector, 3, sizeof(cl_mem), &(matrixCl->pointersCsc));
    clSetKernelArg(env->kernels->matrixProductVector, 4, sizeof(cl_mem), &(matrixCl->indicesCsc));
    clSetKernelArg(env->kernels->matrixProductVector, 5, sizeof(cl_mem), &(matrixCl->valuesCsc));
    clSetKernelArg(env->kernels->matrixProductVector, 6, sizeof(cl_mem), &(matrixCl->type));
    clSetKernelArg(env->kernels->matrixProductVector, 7, sizeof(cl_mem), &clVariables);
    clSetKernelArg(env->kernels->matrixProductVector, 8, sizeof(cl_mem), &clVectorAux);
    clSetKernelArg(env->kernels->matrixProductVector, 9, sizeof(cl_mem), &clTransposeFalse);
	clEnqueueNDRangeKernel(env->queue, env->kernels->matrixProductVector, 1, NULL, global, NULL, 0, NULL, NULL);

	clSetKernelArg(env->kernels->vectorSubtractionVector, 0, sizeof(cl_mem), &clVectorB);
    clSetKernelArg(env->kernels->vectorSubtractionVector, 1, sizeof(cl_mem), &clVectorAux);
    clSetKernelArg(env->kernels->vectorSubtractionVector, 2, sizeof(cl_mem), &clVectorR);
	clEnqueueNDRangeKernel(env->queue, env->kernels->vectorSubtractionVector, 1, NULL, global, NULL, 0, NULL, NULL);

	clSetKernelArg(env->kernels->vectorAttribution, 0, sizeof(cl_mem), &clVectorR2);
	clSetKernelArg(env->kernels->vectorAttribution, 1, sizeof(cl_mem), &clVectorR);
	clEnqueueNDRangeKernel(env->queue, env->kernels->vectorAttribution, 1, NULL, global, NULL, 0, NULL, NULL);

	rho = rho0 = 1; // rho = 1
    clEnqueueWriteBuffer(env->queue, clRho, CL_TRUE, 0, sizeof(double), &rho, 0, NULL, NULL);
    clEnqueueWriteBuffer(env->queue, clRho0, CL_TRUE, 0, sizeof(double), &rho0, 0, NULL, NULL);

	clSetKernelArg(env->kernels->bicgResiduals, 0, sizeof(cl_mem), &clAlpha);
	clSetKernelArg(env->kernels->bicgResiduals, 1, sizeof(cl_mem), &clPartialAlpha);
	clSetKernelArg(env->kernels->bicgResiduals, 2, sizeof(cl_mem), &clGroupQty);
	clSetKernelArg(env->kernels->bicgResiduals, 3, sizeof(cl_mem), &clRho);
	clSetKernelArg(env->kernels->bicgResiduals, 4, sizeof(cl_mem), &clPartialRes);
	clSetKernelArg(env->kernels->bicgResiduals, 5, sizeof(cl_mem), &clRes);

	clSetKernelArg(env->kernels->bicgResiduals, 6, sizeof(cl_mem), &clVariables);
	clSetKernelArg(env->kernels->bicgResiduals, 7, sizeof(cl_mem), &clVectorP);
	clSetKernelArg(env->kernels->bicgResiduals, 8, sizeof(cl_mem), &clVectorR);
	clSetKernelArg(env->kernels->bicgResiduals, 9, sizeof(cl_mem), &clVectorR2);
	clSetKernelArg(env->kernels->bicgResiduals, 10, sizeof(cl_mem), &clVectorV);
	clSetKernelArg(env->kernels->bicgResiduals, 11, sizeof(cl_mem), &clVectorV2);
	clSetKernelArg(env->kernels->bicgResiduals, 12, sizeof(cl_mem), &clConvergence);

	clSetKernelArg(env->kernels->bicgBeta, 0, sizeof(cl_mem), &clBeta);
	clSetKernelArg(env->kernels->bicgBeta, 1, sizeof(cl_mem), &clPartialBeta);
	clSetKernelArg(env->kernels->bicgBeta, 2, sizeof(cl_mem), &clGroupQty);
	clSetKernelArg(env->kernels->bicgBeta, 3, sizeof(cl_mem), &clRho);
	clSetKernelArg(env->kernels->bicgBeta, 4, sizeof(cl_mem), &clRho0);
	clSetKernelArg(env->kernels->bicgBeta, 5, sizeof(cl_mem), &clRes);
	clSetKernelArg(env->kernels->bicgBeta, 6, sizeof(cl_mem), &clConvergedIteration);
	clSetKernelArg(env->kernels->bicgBeta, 7, sizeof(cl_mem), &clEpsilon);
	clSetKernelArg(env->kernels->bicgBeta, 8, sizeof(cl_mem), &clConvergence);

	clSetKernelArg(env->kernels->bicgDirections, 0, sizeof(cl_mem), &clBeta);
	clSetKernelArg(env->kernels->bicgDirections, 1, sizeof(cl_mem), &clVectorP);
	clSetKernelArg(env->kernels->bicgDirections, 2, sizeof(cl_mem), &clVectorP2);
	clSetKernelArg(env->kernels->bicgDirections, 3, sizeof(cl_mem), &clVectorR);
	clSetKernelArg(env->kernels->bicgDirections, 4, sizeof(cl_mem), &clVectorR2);
	clSetKernelArg(env->kernels->bicgDirections, 5, sizeof(cl_mem), &clDiagMatrixPrecond);
	clSetKernelArg(env->kernels->bicgDirections, 6, sizeof(cl_mem), &clPrecond);

	while (result->numIterations < maxIterations) {
		clSetKernelArg(env->kernels->diagonalMatrixProductVector, 0, sizeof(cl_mem), &clDiagMatrixPrecond);
		clSetKernelArg(env->kernels->diagonalMatrixProductVector, 1, sizeof(cl_mem), &clVectorR);
		clSetKernelArg(env->kernels->diagonalMatrixProductVector, 2, sizeof(cl_mem), &clVectorAux);

		clEnqueueNDRangeKernel(env->queue, env->kernels->diagonalMatrixProductVector, 1, NULL, global, NULL, 0, NULL, NULL);


		clSetKernelArg(env->kernels->vectorProductVector, 0, sizeof(cl_mem), &clVectorR2);
		clSetKernelArg(env->kernels->vectorProductVector, 1, sizeof(cl_mem), precond ? &clVectorAux : &clVectorR);
		clSetKernelArg(env->kernels->vectorProductVector, 2, sizeof(cl_mem), &clPartialBeta);
		clSetKernelArg(env->kernels->vectorProductVector, 3, sizeof(double) * workGroupSizeVpv[0], NULL);
		clSetKernelArg(env->kernels->vectorProductVector, 4, sizeof(cl_mem), &clSize);
		clEnqueueNDRangeKernel(env->queue, env->kernels->vectorProductVector, 1, NULL, globalVpv, workGroupSizeVpv, 0, NULL, NULL);

		clEnqueueNDRangeKernel(env->queue, env->kernels->bicgBeta, 1, NULL, globalTotal, NULL, 0, NULL, NULL);

		// p = r(i-1) + beta * p(i-1)
		// p2 = r2(i-1) + beta * p2(i-1)
		clEnqueueNDRangeKernel(env->queue, env->kernels->bicgDirections, 1, NULL, global, NULL, 0, NULL, NULL);

		// v = A*p
		clSetKernelArg(env->kernels->matrixProductVector, 0, sizeof(cl_mem), &(matrixCl->pointersCsr));
		clSetKernelArg(env->kernels->matrixProductVector, 1, sizeof(cl_mem), &(matrixCl->indicesCsr));
		clSetKernelArg(env->kernels->matrixProductVector, 2, sizeof(cl_mem), &(matrixCl->valuesCsr));
		clSetKernelArg(env->kernels->matrixProductVector, 3, sizeof(cl_mem), &(matrixCl->pointersCsc));
		clSetKernelArg(env->kernels->matrixProductVector, 4, sizeof(cl_mem), &(matrixCl->indicesCsc));
		clSetKernelArg(env->kernels->matrixProductVector, 5, sizeof(cl_mem), &(matrixCl->valuesCsc));
		clSetKernelArg(env->kernels->matrixProductVector, 6, sizeof(cl_mem), &(matrixCl->type));
		clSetKernelArg(env->kernels->matrixProductVector, 7, sizeof(cl_mem), &clVectorP);
		clSetKernelArg(env->kernels->matrixProductVector, 8, sizeof(cl_mem), &clVectorV);
		clSetKernelArg(env->kernels->matrixProductVector, 9, sizeof(cl_mem), &clTransposeFalse);
		clEnqueueNDRangeKernel(env->queue, env->kernels->matrixProductVector, 1, NULL, global, NULL, 0, NULL, NULL);

		// v2 = A' * p2
		clSetKernelArg(env->kernels->matrixProductVector, 7, sizeof(cl_mem), &clVectorP2);
		clSetKernelArg(env->kernels->matrixProductVector, 8, sizeof(cl_mem), &clVectorV2);
		clSetKernelArg(env->kernels->matrixProductVector, 9, sizeof(cl_mem), &clTransposeTrue);
		clEnqueueNDRangeKernel(env->queue, env->kernels->matrixProductVector, 1, NULL, global, NULL, 0, NULL, NULL);

		// partialAlpha = (p2 * v)
 		clSetKernelArg(env->kernels->vectorProductVector, 0, sizeof(cl_mem), &clVectorP2);
		clSetKernelArg(env->kernels->vectorProductVector, 1, sizeof(cl_mem), &clVectorV);
		clSetKernelArg(env->kernels->vectorProductVector, 2, sizeof(cl_mem), &clPartialAlpha);
		clSetKernelArg(env->kernels->vectorProductVector, 3, sizeof(double) * workGroupSizeVpv[0], NULL);
		clSetKernelArg(env->kernels->vectorProductVector, 4, sizeof(cl_mem), &clSize);
		clEnqueueNDRangeKernel(env->queue, env->kernels->vectorProductVector, 1, NULL, globalVpv, workGroupSizeVpv, 0, NULL, NULL);

		// partialRes = R * R
		clSetKernelArg(env->kernels->vectorProductVector, 0, sizeof(cl_mem), &clVectorR);
		clSetKernelArg(env->kernels->vectorProductVector, 1, sizeof(cl_mem), &clVectorR);
		clSetKernelArg(env->kernels->vectorProductVector, 2, sizeof(cl_mem), &clPartialRes);
		clSetKernelArg(env->kernels->vectorProductVector, 3, sizeof(double) * workGroupSizeVpv[0], NULL);
		clSetKernelArg(env->kernels->vectorProductVector, 4, sizeof(cl_mem), &clSize);
		clEnqueueNDRangeKernel(env->queue, env->kernels->vectorProductVector, 1, NULL, globalVpv, workGroupSizeVpv, 0, NULL, NULL);

		/*
			alpha = rho / sum(partialAlpha)
			x = x(i-1) + a * p
			r = r(i-1) - a * v
			r2 = r2(i-1) - a * v2

			res = sum(partialRes)
		*/
		clEnqueueNDRangeKernel(env->queue, env->kernels->bicgResiduals, 1, NULL, global, NULL, 0, NULL, NULL);

		result->numIterations++;

		if (result->numIterations % 50 == 0) {
			clEnqueueReadBuffer(env->queue, clRes, CL_TRUE, 0, sizeof(double), &res, 0, NULL, NULL);
			if (res <= epsilon * epsilon) {
				break;
			}
		}
	}
	clEnqueueReadBuffer(env->queue, clConvergedIteration, CL_TRUE, 0, sizeof(unsigned int), &(result->convergedIteration), 0, NULL, NULL);
	clEnqueueReadBuffer(env->queue, clVariables, CL_TRUE, 0, sizeof(double) * numVariables, result->variables, 0, NULL, NULL);

	clFinish(env->queue);

	CG_release_alloc_matrix_cl(matrixCl);
	clReleaseMemObject(clVariables);
	clReleaseMemObject(clVectorB);
	clReleaseMemObject(clVectorAux);
	clReleaseMemObject(clVectorR);
	clReleaseMemObject(clVectorR2);
	clReleaseMemObject(clVectorP);
	clReleaseMemObject(clVectorP2);
	clReleaseMemObject(clRho);
	clReleaseMemObject(clRho0);
	clReleaseMemObject(clRes);
	clReleaseMemObject(clAlpha);
	clReleaseMemObject(clBeta);
	clReleaseMemObject(clSize);
	clReleaseMemObject(clGroupResult);
	clReleaseMemObject(clGroupQty);
	if (precond) {
		if (precondMatrix->type == HB) {
			CG_release_alloc_matrix_cl(precondMatrixCl);
		}
	}
}


void CG_conjugate_gradient_cl(HB_Matrix *matrix,
							HB_Matrix_CL *matrixCl,
							double *vectorB,
							CG_Result *result,
							unsigned int maxIterations,
							double epsilon,
							PC_Matrix *precondMatrix,
							CL_Enviroment *env)
{
	unsigned int numVariables = matrix->sizePointers - 1, precond = precondMatrix != NULL, convergence = 0;
	double newSigma, sigma0;

	cl_mem clDiagMatrixPrecond;
	HB_Matrix_CL *precondMatrixCl = malloc(sizeof(HB_Matrix_CL));
	if (precond) {
		CG_alloc_precond_matrix_cl(env, precondMatrix, &clDiagMatrixPrecond, precondMatrixCl);
	}

	size_t global[] = {numVariables, 1};

	cl_mem clVariables = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorB = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorAux = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorR = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorD = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorQ = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	cl_mem clVectorS;

	if (precond) {
		clVectorS = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double) * numVariables, NULL, NULL);
	}

	cl_mem clNewSigma = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clSigma0 = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clOldSigma = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clAlpha = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clBeta = clCreateBuffer(env->context,  CL_MEM_READ_WRITE,  sizeof(double), NULL, NULL);
	cl_mem clSize = clCreateBuffer(env->context,  CL_MEM_READ_ONLY, sizeof(unsigned int), NULL, NULL);
	cl_mem clNumIterations = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(unsigned int), NULL, NULL);
	cl_mem clEpsilon = clCreateBuffer(env->context,  CL_MEM_READ_ONLY, sizeof(double), NULL, NULL);
	cl_mem clConvergence = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(unsigned int), NULL, NULL);

	clEnqueueWriteBuffer(env->queue, clVectorB, CL_TRUE, 0, sizeof(double) * numVariables, vectorB, 0, NULL, NULL);
	clEnqueueWriteBuffer(env->queue, clSize, CL_TRUE, 0, sizeof(unsigned int), &numVariables, 0, NULL, NULL);

	size_t workGroupSizeVpv[] = {numVariables <= MAX_WORK_GROUP_SIZE ? MIN_WORK_GROUP_SIZE : MAX_WORK_GROUP_SIZE, 1};
    size_t globalVpv[] = {((numVariables / workGroupSizeVpv[0]) * workGroupSizeVpv[0]) + workGroupSizeVpv[0], 1};
	cl_mem clGroupResult = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(double) * (numVariables / workGroupSizeVpv[0]), NULL, NULL);
	cl_mem clPartialAlpha = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(double) * (numVariables / workGroupSizeVpv[0]), NULL, NULL);
	cl_mem clPartialSigma = clCreateBuffer(env->context,  CL_MEM_READ_WRITE, sizeof(double) * (numVariables / workGroupSizeVpv[0]), NULL, NULL);
	unsigned int groupQty = globalVpv[0] / workGroupSizeVpv[0];
	cl_mem clGroupQty = clCreateBuffer(env->context,  CL_MEM_READ_ONLY, sizeof(unsigned int), NULL, NULL);
	clEnqueueWriteBuffer(env->queue, clGroupQty, CL_TRUE, 0, sizeof(unsigned int), &groupQty, 0, NULL, NULL);
	size_t globalTotal[] = {1, 1};

	cl_kernel zeroVectors = CL_create_kernel(env, "zero_vector");
    clSetKernelArg(zeroVectors, 0, sizeof(cl_mem), &clVariables);
	clEnqueueNDRangeKernel(env->queue, zeroVectors, 1, NULL, global, NULL, 0, NULL, NULL);

	unsigned int transpose = 0;
    cl_mem clTranspose = clCreateBuffer(env->context, CL_MEM_READ_ONLY, sizeof(unsigned int), NULL, NULL);
    clEnqueueWriteBuffer(env->queue, clTranspose, CL_TRUE, 0, sizeof(unsigned int), &transpose, 0, NULL, NULL);

	clEnqueueWriteBuffer(env->queue, clNumIterations, CL_TRUE, 0, sizeof(unsigned int), &(result->numIterations), 0, NULL, NULL);
	clEnqueueWriteBuffer(env->queue, clEpsilon, CL_TRUE, 0, sizeof(double), &epsilon, 0, NULL, NULL);
	clEnqueueWriteBuffer(env->queue, clConvergence, CL_TRUE, 0, sizeof(unsigned int), &convergence, 0, NULL, NULL);

	clSetKernelArg(env->kernels->matrixProductVector, 0, sizeof(cl_mem), &(matrixCl->pointersCsr));
    clSetKernelArg(env->kernels->matrixProductVector, 1, sizeof(cl_mem), &(matrixCl->indicesCsr));
    clSetKernelArg(env->kernels->matrixProductVector, 2, sizeof(cl_mem), &(matrixCl->valuesCsr));
    clSetKernelArg(env->kernels->matrixProductVector, 3, sizeof(cl_mem), &(matrixCl->pointersCsc));
    clSetKernelArg(env->kernels->matrixProductVector, 4, sizeof(cl_mem), &(matrixCl->indicesCsc));
    clSetKernelArg(env->kernels->matrixProductVector, 5, sizeof(cl_mem), &(matrixCl->valuesCsc));
    clSetKernelArg(env->kernels->matrixProductVector, 6, sizeof(cl_mem), &(matrixCl->type));
    clSetKernelArg(env->kernels->matrixProductVector, 7, sizeof(cl_mem), &clVariables);
    clSetKernelArg(env->kernels->matrixProductVector, 8, sizeof(cl_mem), &clVectorAux);
    clSetKernelArg(env->kernels->matrixProductVector, 9, sizeof(cl_mem), &clTranspose);
	clEnqueueNDRangeKernel(env->queue, env->kernels->matrixProductVector, 1, NULL, global, NULL, 0, NULL, NULL);

	clSetKernelArg(env->kernels->vectorSubtractionVector, 0, sizeof(cl_mem), &clVectorB);
    clSetKernelArg(env->kernels->vectorSubtractionVector, 1, sizeof(cl_mem), &clVectorAux);
    clSetKernelArg(env->kernels->vectorSubtractionVector, 2, sizeof(cl_mem), &clVectorR);
	clEnqueueNDRangeKernel(env->queue, env->kernels->vectorSubtractionVector, 1, NULL, global, NULL, 0, NULL, NULL);

	if (precond) {
		CG_precond_matrix_product_vector_cl(env, precondMatrix, precondMatrixCl, &clDiagMatrixPrecond, &clVectorR, &clVectorD, numVariables);
	} else {
		clSetKernelArg(env->kernels->vectorAttribution, 0, sizeof(cl_mem), &clVectorD);
    	clSetKernelArg(env->kernels->vectorAttribution, 1, sizeof(cl_mem), &clVectorR);
		clEnqueueNDRangeKernel(env->queue, env->kernels->vectorAttribution, 1, NULL, global, NULL, 0, NULL, NULL);
	}
	clSetKernelArg(env->kernels->vectorProductVector, 0, sizeof(cl_mem), &clVectorR);
    clSetKernelArg(env->kernels->vectorProductVector, 1, sizeof(cl_mem), precond ? &clVectorD : &clVectorR);
    clSetKernelArg(env->kernels->vectorProductVector, 2, sizeof(cl_mem), &clGroupResult);
    clSetKernelArg(env->kernels->vectorProductVector, 3, sizeof(double) * workGroupSizeVpv[0], NULL);
    clSetKernelArg(env->kernels->vectorProductVector, 4, sizeof(cl_mem), &clSize);
	clEnqueueNDRangeKernel(env->queue, env->kernels->vectorProductVector, 1, NULL, globalVpv, workGroupSizeVpv, 0, NULL, NULL);
	clSetKernelArg(env->kernels->cgSigma0, 0, sizeof(cl_mem), &clGroupResult);
    clSetKernelArg(env->kernels->cgSigma0, 1, sizeof(cl_mem), &clNewSigma);
    clSetKernelArg(env->kernels->cgSigma0, 2, sizeof(cl_mem), &clGroupQty);
    clSetKernelArg(env->kernels->cgSigma0, 3, sizeof(cl_mem), &clSigma0);
	clEnqueueNDRangeKernel(env->queue, env->kernels->cgSigma0, 1, NULL, globalTotal, NULL, 0, NULL, NULL);
	clEnqueueReadBuffer(env->queue, clSigma0, CL_TRUE, 0, sizeof(double), &sigma0, 0, NULL, NULL);

	while (result->numIterations < maxIterations) {
		clSetKernelArg(env->kernels->matrixProductVector, 0, sizeof(cl_mem), &(matrixCl->pointersCsr));
		clSetKernelArg(env->kernels->matrixProductVector, 1, sizeof(cl_mem), &(matrixCl->indicesCsr));
		clSetKernelArg(env->kernels->matrixProductVector, 2, sizeof(cl_mem), &(matrixCl->valuesCsr));
		clSetKernelArg(env->kernels->matrixProductVector, 3, sizeof(cl_mem), &(matrixCl->pointersCsc));
		clSetKernelArg(env->kernels->matrixProductVector, 4, sizeof(cl_mem), &(matrixCl->indicesCsc));
		clSetKernelArg(env->kernels->matrixProductVector, 5, sizeof(cl_mem), &(matrixCl->valuesCsc));
    	clSetKernelArg(env->kernels->matrixProductVector, 6, sizeof(cl_mem), &(matrixCl->type));
		clSetKernelArg(env->kernels->matrixProductVector, 7, sizeof(cl_mem), &clVectorD);
    	clSetKernelArg(env->kernels->matrixProductVector, 8, sizeof(cl_mem), &clVectorQ);
		clEnqueueNDRangeKernel(env->queue, env->kernels->matrixProductVector, 1, NULL, global, NULL, 0, NULL, NULL);

		clSetKernelArg(env->kernels->vectorProductVector, 0, sizeof(cl_mem), &clVectorD);
		clSetKernelArg(env->kernels->vectorProductVector, 1, sizeof(cl_mem), &clVectorQ);
		clSetKernelArg(env->kernels->vectorProductVector, 2, sizeof(cl_mem), &clPartialAlpha);
		clSetKernelArg(env->kernels->vectorProductVector, 3, sizeof(double) * workGroupSizeVpv[0], NULL);
		clSetKernelArg(env->kernels->vectorProductVector, 4, sizeof(cl_mem), &clSize);
		clEnqueueNDRangeKernel(env->queue, env->kernels->vectorProductVector, 1, NULL, globalVpv, workGroupSizeVpv, 0, NULL, NULL);

		clSetKernelArg(env->kernels->cgAlpha, 0, sizeof(cl_mem), &clPartialAlpha);
		clSetKernelArg(env->kernels->cgAlpha, 1, sizeof(cl_mem), &clGroupQty);
		clSetKernelArg(env->kernels->cgAlpha, 2, sizeof(cl_mem), &clAlpha);
		clSetKernelArg(env->kernels->cgAlpha, 3, sizeof(cl_mem), &clNewSigma);
		clSetKernelArg(env->kernels->cgAlpha, 4, sizeof(cl_mem), &clVariables);
		clSetKernelArg(env->kernels->cgAlpha, 5, sizeof(cl_mem), &clVectorD);
		clSetKernelArg(env->kernels->cgAlpha, 6, sizeof(cl_mem), &clConvergence);
		clEnqueueNDRangeKernel(env->queue, env->kernels->cgAlpha, 1, NULL, global, NULL, 0, NULL, NULL);

		if (result->numIterations % 50 == 0) {
			clSetKernelArg(env->kernels->matrixProductVector, 0, sizeof(cl_mem), &(matrixCl->pointersCsr));
			clSetKernelArg(env->kernels->matrixProductVector, 1, sizeof(cl_mem), &(matrixCl->indicesCsr));
			clSetKernelArg(env->kernels->matrixProductVector, 2, sizeof(cl_mem), &(matrixCl->valuesCsr));
			clSetKernelArg(env->kernels->matrixProductVector, 3, sizeof(cl_mem), &(matrixCl->pointersCsc));
			clSetKernelArg(env->kernels->matrixProductVector, 4, sizeof(cl_mem), &(matrixCl->indicesCsc));
			clSetKernelArg(env->kernels->matrixProductVector, 5, sizeof(cl_mem), &(matrixCl->valuesCsc));
			clSetKernelArg(env->kernels->matrixProductVector, 6, sizeof(cl_mem), &(matrixCl->type));
			clSetKernelArg(env->kernels->matrixProductVector, 7, sizeof(cl_mem), &clVariables);
			clSetKernelArg(env->kernels->matrixProductVector, 8, sizeof(cl_mem), &clVectorAux);
			clEnqueueNDRangeKernel(env->queue, env->kernels->matrixProductVector, 1, NULL, global, NULL, 0, NULL, NULL);

			clSetKernelArg(env->kernels->vectorSubtractionVector, 0, sizeof(cl_mem), &clVectorB);
			clSetKernelArg(env->kernels->vectorSubtractionVector, 1, sizeof(cl_mem), &clVectorAux);
			clSetKernelArg(env->kernels->vectorSubtractionVector, 2, sizeof(cl_mem), &clVectorR);
			clEnqueueNDRangeKernel(env->queue, env->kernels->vectorSubtractionVector, 1, NULL, global, NULL, 0, NULL, NULL);
		} else {
			clSetKernelArg(env->kernels->vectorSubScalarProductVector, 0, sizeof(cl_mem), &clVectorR);
			clSetKernelArg(env->kernels->vectorSubScalarProductVector, 1, sizeof(cl_mem), &clVectorQ);
			clSetKernelArg(env->kernels->vectorSubScalarProductVector, 2, sizeof(cl_mem), &clVectorR);
			clSetKernelArg(env->kernels->vectorSubScalarProductVector, 3, sizeof(cl_mem), &clAlpha);
			clEnqueueNDRangeKernel(env->queue, env->kernels->vectorSubScalarProductVector, 1, NULL, global, NULL, 0, NULL, NULL);
		}

		if (precond) {
			CG_precond_matrix_product_vector_cl(env, precondMatrix, precondMatrixCl, &clDiagMatrixPrecond, &clVectorR, &clVectorS, numVariables);
		}

		clSetKernelArg(env->kernels->vectorProductVector, 0, sizeof(cl_mem), &clVectorR);
		clSetKernelArg(env->kernels->vectorProductVector, 1, sizeof(cl_mem), precond ? &clVectorS : &clVectorR);
		clSetKernelArg(env->kernels->vectorProductVector, 2, sizeof(cl_mem), &clPartialSigma);
		clSetKernelArg(env->kernels->vectorProductVector, 3, sizeof(double) * workGroupSizeVpv[0], NULL);
		clSetKernelArg(env->kernels->vectorProductVector, 4, sizeof(cl_mem), &clSize);
		clEnqueueNDRangeKernel(env->queue, env->kernels->vectorProductVector, 1, NULL, globalVpv, workGroupSizeVpv, 0, NULL, NULL);

		clSetKernelArg(env->kernels->cgBeta, 0, sizeof(cl_mem), &clPartialSigma);
		clSetKernelArg(env->kernels->cgBeta, 1, sizeof(cl_mem), &clGroupQty);
		clSetKernelArg(env->kernels->cgBeta, 2, sizeof(cl_mem), &clNewSigma);
		clSetKernelArg(env->kernels->cgBeta, 3, sizeof(cl_mem), &clOldSigma);
		clSetKernelArg(env->kernels->cgBeta, 4, sizeof(cl_mem), &clBeta);
		clSetKernelArg(env->kernels->cgBeta, 5, sizeof(cl_mem), &clNumIterations);
		clSetKernelArg(env->kernels->cgBeta, 6, sizeof(cl_mem), &clEpsilon);
		clSetKernelArg(env->kernels->cgBeta, 7, sizeof(cl_mem), &clSigma0);
		clSetKernelArg(env->kernels->cgBeta, 8, sizeof(cl_mem), &clConvergence);
		clEnqueueNDRangeKernel(env->queue, env->kernels->cgBeta, 1, NULL, globalTotal, NULL, 0, NULL, NULL);

		clSetKernelArg(env->kernels->vectorSumScalarProductVector, 0, sizeof(cl_mem), precond ? &clVectorS : &clVectorR);
		clSetKernelArg(env->kernels->vectorSumScalarProductVector, 1, sizeof(cl_mem), &clVectorD);
		clSetKernelArg(env->kernels->vectorSumScalarProductVector, 2, sizeof(cl_mem), &clVectorD);
		clSetKernelArg(env->kernels->vectorSumScalarProductVector, 3, sizeof(cl_mem), &clBeta);
		clEnqueueNDRangeKernel(env->queue, env->kernels->vectorSumScalarProductVector, 1, NULL, global, NULL, 0, NULL, NULL);

		result->numIterations++;
		if (result->numIterations % 50 == 0) {
			clEnqueueReadBuffer(env->queue, clNewSigma, CL_TRUE, 0, sizeof(double), &newSigma, 0, NULL, NULL);
			if (newSigma <= epsilon * epsilon * sigma0) {
				break;
			}
		}
	}

	clEnqueueReadBuffer(env->queue, clNumIterations, CL_TRUE, 0, sizeof(unsigned int), &(result->convergedIteration), 0, NULL, NULL);
	clEnqueueReadBuffer(env->queue, clVariables, CL_TRUE, 0, sizeof(double) * numVariables, result->variables, 0, NULL, NULL);

	clFinish(env->queue);
	CG_release_alloc_matrix_cl(matrixCl);
	clReleaseMemObject(clVariables);
	clReleaseMemObject(clVectorB);
	clReleaseMemObject(clVectorAux);
	clReleaseMemObject(clVectorR);
	clReleaseMemObject(clVectorD);
	clReleaseMemObject(clVectorQ);
	clReleaseMemObject(clNewSigma);
	clReleaseMemObject(clSigma0);
	clReleaseMemObject(clOldSigma);
	clReleaseMemObject(clAlpha);
	clReleaseMemObject(clBeta);
	clReleaseMemObject(clSize);
	clReleaseMemObject(clGroupResult);
	clReleaseMemObject(clGroupQty);
	if (precond) {
		clReleaseMemObject(clVectorS);
		if (precondMatrix->type == HB) {
			CG_release_alloc_matrix_cl(precondMatrixCl);
		}
	}
}

void CG_alloc_precond_matrix_cl(CL_Enviroment *env, PC_Matrix *precondMatrix, cl_mem *clDiagMatrixPrecond, HB_Matrix_CL *precondMatrixCl) {
	HB_Matrix *precondMatrixCsr;

	if (precondMatrix->type == HB) {
		precondMatrixCsr = HB_csc2csr(precondMatrix->hbMatrix);
	}

	switch (precondMatrix->type) {
		case DIAG:
			*clDiagMatrixPrecond = clCreateBuffer(env->context,  CL_MEM_READ_ONLY,  sizeof(double) * precondMatrix->daMatrix->size, NULL, NULL);
			clEnqueueWriteBuffer(env->queue, *clDiagMatrixPrecond, CL_TRUE, 0,
										sizeof(double) * precondMatrix->daMatrix->size, precondMatrix->daMatrix->values,
										0, NULL, NULL);

			break;
		case HB:
			CG_alloc_matrix_cl(env, precondMatrixCl, precondMatrixCsr, precondMatrix->hbMatrix);

			break;
	}
}

void CG_precond_matrix_product_vector_cl(CL_Enviroment *env,
											PC_Matrix *matrix,
											HB_Matrix_CL *precondMatrixCl,
											cl_mem *clDiagMatrixPrecond,
											cl_mem *clVector,
											cl_mem *clResult,
											unsigned int size
) {
	size_t globalSize[] = {size, 1};

	switch (matrix->type) {
		case DIAG:
			clSetKernelArg(env->kernels->diagonalMatrixProductVector, 0, sizeof(cl_mem), clDiagMatrixPrecond);
			clSetKernelArg(env->kernels->diagonalMatrixProductVector, 1, sizeof(cl_mem), clVector);
			clSetKernelArg(env->kernels->diagonalMatrixProductVector, 2, sizeof(cl_mem), clResult);

			clEnqueueNDRangeKernel(env->queue, env->kernels->diagonalMatrixProductVector, 1, NULL, globalSize, NULL, 0, NULL, NULL);
			break;
		case HB:
			clSetKernelArg(env->kernels->matrixProductVector, 0, sizeof(cl_mem), &(precondMatrixCl->pointersCsr));
			clSetKernelArg(env->kernels->matrixProductVector, 1, sizeof(cl_mem), &(precondMatrixCl->indicesCsr));
			clSetKernelArg(env->kernels->matrixProductVector, 2, sizeof(cl_mem), &(precondMatrixCl->valuesCsr));
			clSetKernelArg(env->kernels->matrixProductVector, 3, sizeof(cl_mem), &(precondMatrixCl->pointersCsc));
			clSetKernelArg(env->kernels->matrixProductVector, 4, sizeof(cl_mem), &(precondMatrixCl->indicesCsc));
			clSetKernelArg(env->kernels->matrixProductVector, 5, sizeof(cl_mem), &(precondMatrixCl->valuesCsc));
			clSetKernelArg(env->kernels->matrixProductVector, 6, sizeof(cl_mem), &(precondMatrixCl->type));
			clSetKernelArg(env->kernels->matrixProductVector, 7, sizeof(cl_mem), clVector);
			clSetKernelArg(env->kernels->matrixProductVector, 8, sizeof(cl_mem), clResult);
			clEnqueueNDRangeKernel(env->queue, env->kernels->matrixProductVector, 1, NULL, globalSize, NULL, 0, NULL, NULL);

			break;
	}
}

HB_Matrix_CL *CG_alloc_matrix_cl(CL_Enviroment *clEnv, HB_Matrix_CL *matrixCl, HB_Matrix *matrixCsr, HB_Matrix *matrixCsc) {
	matrixCl->pointersCsr = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY,  sizeof(int) * matrixCsr->sizePointers, NULL, NULL);
	matrixCl->indicesCsr = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY,  sizeof(int) * matrixCsr->sizeIndices, NULL, NULL);
	matrixCl->valuesCsr = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY,  sizeof(double) * matrixCsr->sizeValues, NULL, NULL);

    matrixCl->pointersCsc = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY,  sizeof(int) * matrixCsc->sizePointers, NULL, NULL);
	matrixCl->indicesCsc = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY,  sizeof(int) * matrixCsc->sizeIndices, NULL, NULL);
	matrixCl->valuesCsc = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY,  sizeof(double) * matrixCsc->sizeValues, NULL, NULL);
	matrixCl->type = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY,  sizeof(char), NULL, NULL);

	if (!matrixCl->pointersCsr || !matrixCl->pointersCsr || !matrixCl->valuesCsr) {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }

    clEnqueueWriteBuffer(clEnv->queue, matrixCl->pointersCsr, CL_TRUE, 0, sizeof(int) * matrixCsr->sizePointers, matrixCsr->pointers, 0, NULL, NULL);
	clEnqueueWriteBuffer(clEnv->queue, matrixCl->indicesCsr, CL_TRUE, 0, sizeof(int) * matrixCsr->sizeIndices, matrixCsr->indices, 0, NULL, NULL);
	clEnqueueWriteBuffer(clEnv->queue, matrixCl->valuesCsr, CL_TRUE, 0, sizeof(double) * matrixCsr->sizeValues, matrixCsr->values, 0, NULL, NULL);

    clEnqueueWriteBuffer(clEnv->queue, matrixCl->pointersCsc, CL_TRUE, 0, sizeof(int) * matrixCsc->sizePointers, matrixCsc->pointers, 0, NULL, NULL);
	clEnqueueWriteBuffer(clEnv->queue, matrixCl->indicesCsc, CL_TRUE, 0, sizeof(int) * matrixCsc->sizeIndices, matrixCsc->indices, 0, NULL, NULL);
	clEnqueueWriteBuffer(clEnv->queue, matrixCl->valuesCsc, CL_TRUE, 0, sizeof(double) * matrixCsc->sizeValues, matrixCsc->values, 0, NULL, NULL);

	clEnqueueWriteBuffer(clEnv->queue, matrixCl->type, CL_TRUE, 0, sizeof(char), &(matrixCsc->type), 0, NULL, NULL);

    return matrixCl;
}

void CG_release_alloc_matrix_cl(HB_Matrix_CL *matrixCl) {
	clReleaseMemObject(matrixCl->pointersCsr);
	clReleaseMemObject(matrixCl->indicesCsr);
	clReleaseMemObject(matrixCl->valuesCsr);
	clReleaseMemObject(matrixCl->pointersCsc);
	clReleaseMemObject(matrixCl->indicesCsc);
	clReleaseMemObject(matrixCl->valuesCsc);
	clReleaseMemObject(matrixCl->type);
}

