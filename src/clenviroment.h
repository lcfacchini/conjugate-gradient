#ifndef CLENVIROMENT_H
#define CLENVIROMENT_H

#include <stdio.h>
#include <libgen.h>

#define CL_TARGET_OPENCL_VERSION 220
#include <CL/cl.h>
#include "hbmatrix.h"

#define MIN_WORK_GROUP_SIZE 32
#define MAX_WORK_GROUP_SIZE 256

typedef struct {
    cl_kernel matrixProductVector;
    cl_kernel diagonalMatrixProductVector;
    cl_kernel vectorProductVector;
    cl_kernel vectorSubtractionVector;
    cl_kernel vectorSumScalarProductVector;
    cl_kernel vectorSubScalarProductVector;
    cl_kernel vectorAttribution;
    cl_kernel cgSigma0;
    cl_kernel cgBeta;
    cl_kernel cgAlpha;
    cl_kernel bicgResiduals;
    cl_kernel bicgDirections;
    cl_kernel bicgBeta;
} CL_Kernels;

typedef struct {
    cl_device_id deviceId;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    CL_Kernels *kernels;
} CL_Enviroment;

typedef struct {
    cl_mem clVectorR;
    cl_mem clVectorD;
    cl_mem clVectorQ;
    cl_mem clVectorS;
    cl_mem clVectorAux;
} CLCG_Data;

CL_Enviroment *CL_start_env();

char *CL_read_kernel_file(char *kernelFilePath);

cl_kernel CL_create_kernel(CL_Enviroment *clEnv,  char *kernelName);

void CLHB_matrix_product_vector(CL_Enviroment *clEnv, cl_kernel kernel, HB_Matrix *matrix, HB_Matrix_CL *clhbMatrix, double *vector, double *result, unsigned int transpose);

double CLCG_vector_product_vector(CL_Enviroment *clEnv, cl_kernel kernel, double *v1, double *v2, unsigned int size);

HB_Matrix_CL *alloc_matrix(CL_Enviroment *clEnv, HB_Matrix *matrixCsr, HB_Matrix *matrixCsc);

void CLCG_vector_op_vector(CL_Enviroment *clEnv,
                                cl_kernel kernel,
                                double *v1,
                                double *v2,
                                double *result,
                                unsigned int size);

void CL_prepare_kernels(CL_Enviroment *env);

#endif