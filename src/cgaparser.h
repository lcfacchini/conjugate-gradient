#ifndef CGAPARSER_H
#define CGAPARSER_H

#include "cg.h"
#include "precond.h"
#include <argp.h>
#include <libgen.h>
#include <stdlib.h>

#define CGA_PRINT_VARS 'v'

#define CGA_PRINT_ACTUAL_B 'a'

#define CGA_PRINT_DIFF_B 2

#define CGA_PRINT_SIMPLIFIED 3

#define CGA_MAX_ITERATIONS 'i'

#define CGA_EPSILON 'e'

#define CGA_METHOD_CG 'c'

#define CGA_METHOD_BICG 'b'

#define CGA_PRECOND 'p'

#define CGA_PRECOND_OPT_DIAGONAL 'D'

#define CGA_PRECOND_OPT_POLYNOMIAL 'P'

#define CGA_USE_OPENCL 1

typedef struct {
	char **files;
	unsigned int printValueVariables;
	unsigned int printActualB;
	unsigned int printfDiffB;
	unsigned int printfSimplified;
	unsigned int maxIterations;
	double epsilon;
	enum CG_Method method;
	enum PC_Preconditioner preconditioner;
	unsigned int cl;
} CGA_Argument;

CGA_Argument *CGA_parse(int const argc, char *argv[]);

error_t CGA_parser(int key, char *arg, struct argp_state *state);

void CGA_argument_free(CGA_Argument *argument);

#endif
