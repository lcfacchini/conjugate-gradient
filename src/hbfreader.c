#include <string.h>
#include <stdlib.h>
#include "hbfreader.h"

HB_Matrix *HBFR_read(char *path) {
	FILE *file = fopen(path, "r");

	if (file == NULL) {
		return NULL;
	}

	HBFR_Header header;
	HBFR_read_header(file, &header);

	HB_Matrix *matrix = malloc(sizeof(HB_Matrix));
	matrix->type = header.mxtype[1];
	matrix->format = HB_MATRIX_CSC;
	matrix->path = path;

	HBFR_read_pointers(file, &header, matrix);
	HBFR_read_indices(file, &header, matrix);
	HBFR_read_numerical_values(file, &header, matrix);

	fclose(file);

	return matrix;
}

void HBFR_read_header(FILE *file, HBFR_Header *h) {
	fseek(file, HBFR_LINE_WIDTH, SEEK_SET);
	fscanf(file, "%d %d %d %d %d\n", &h->totcrd, &h->ptrcrd, &h->indcrd, &h->valcrd, &h->rhscrd);
	fscanf(file, "%s %d %d %d %d\n", h->mxtype, &h->nrow, &h->ncol, &h->nnzero, &h->neltvl);
	fscanf(file, "(%d%*c%d) (%d%*c%d) (%d%*c%d) (%*d%*c%*f)\n",
		&h->ptrfmtcol, &h->ptrfmtsize, &h->indfmtcol, &h->indfmtsize, &h->valfmtcol, &h->valfmtsize);

	fseek(file, HBFR_LINE_WIDTH * (h->rhscrd > 0 ? 5 : 4), SEEK_SET);
}

void HBFR_read_pointers(FILE *file, HBFR_Header *header, HB_Matrix *matrix) {
	matrix->sizePointers = header->ncol + 1;
	HBFR_Block_Info info = {matrix->sizePointers, header->ptrfmtcol, header->ptrfmtsize, sizeof(unsigned int), "%d"};
	matrix->pointers = HBFR_read_block(file, &info);
}

void HBFR_read_indices(FILE *file, HBFR_Header *header, HB_Matrix *matrix) {
	matrix->sizeIndices = header->nnzero;
	HBFR_Block_Info info = {matrix->sizeIndices, header->indfmtcol, header->indfmtsize, sizeof(unsigned int), "%d"};
	matrix->indices = HBFR_read_block(file, &info);
}

void HBFR_read_numerical_values(FILE *file, HBFR_Header *header, HB_Matrix *matrix) {
	matrix->sizeValues = header->nnzero;
	HBFR_Block_Info info = {matrix->sizeValues, header->valfmtcol, header->valfmtsize, sizeof(double), "%lf"};
	matrix->values = HBFR_read_block(file, &info);
}

void *HBFR_read_block(FILE *file, HBFR_Block_Info *info) {
	void *ptr = malloc(info->totalDataElements * info->elementTypeSize);
	char *buffer = malloc(sizeof(char) * (info->elementSize + 1));

	unsigned int countReadElements;
	for (countReadElements = 0; countReadElements < info->totalDataElements; countReadElements++) {
		fread(buffer, sizeof(char), info->elementSize, file);
		buffer[info->elementSize] = '\0';
		sscanf(buffer, info->elementFormat, ptr + countReadElements * info->elementTypeSize);
		if ((countReadElements + 1) % info->totalDataRowElements == 0) {
			HBFR_ignore_spaces_no_data_end_line(file);
		}
	}

	HBFR_ignore_spaces_no_data_end_line(file);

	free(buffer);

	return ptr;
}

void HBFR_ignore_spaces_no_data_end_line(FILE *file) {
	unsigned int nextLinePosDiff = ftell(file) % HBFR_LINE_WIDTH;
	if (nextLinePosDiff > 0) {
		fseek(file, HBFR_LINE_WIDTH - nextLinePosDiff, SEEK_CUR);
	}
}