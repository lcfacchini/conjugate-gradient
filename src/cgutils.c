#include "cgutils.h"

void CGU_show_result(CG_Result *result) {


	CGU_Time* time = CGU_formated_time(result->time);

	char methodName[30];
	sprintf(
		methodName,
		"%s%s %s %s",
		result->precond ? "P" : "",
		result->method == BICG ? "BiCG" : "CG",
		result->precond ? (result->preconditioner == DIAGONAL ? "Diag" : "Poly") : "",
		result->cl ? "OpenCL" : ""
	);

	char iterations[20];
	if (result->convergedIteration != result->numIterations) {
		sprintf(iterations, "(%d) %d", result->convergedIteration, result->numIterations);
	} else {
		sprintf(iterations, "%d", result->numIterations);
	}

	if (result->printSimplified) {
		printf(
			"%s; \t %s (%5dx%5d) %6d; \t %10d; \t %017.6lf; \t %.2lf%% \n",
			methodName,
			basename(result->matrix->path),
			result->matrix->sizePointers,
			result->matrix->sizePointers,
			result->matrix->sizeValues,
			result->numIterations,
			result->time,
			result->convergencePercent * 100
		);
	} else {
		CGU_print_div("╔", "╦", "╗");
		printf("║ %-20s ║ %-20s ║ %-17s ║ %13s ║ %13s ║ %-17s ║ %-18s ║\n", "File", "Matrix", "Method", "Iterations", "Convergence", "Time in seconds", "Time");
		printf(
			"║ %-20s ║ (%5dx%5d) %6d ║ %-17s ║ %13s ║ %12.2lf%% ║ %017.6lf ║ %02ldh %02ld\' %02ld\'\' %03ldms ║\n",
			basename(result->matrix->path),
			result->matrix->sizePointers,
			result->matrix->sizePointers,
			result->matrix->sizeValues,
			methodName,
			iterations,
			result->convergencePercent * 100,
			result->time,
			time->hour,
			time->minute,
			time->second,
			time->ms
		);

		CGU_print_div("╚", "╩", "╝");
	}

}

void CGU_print_div(char const *left, char const *middle, char const *right) {
	printf(
		"%s══════════════════════%s══════════════════════%s═══════════════════%s═══════════════%s═══════════════%s═══════════════════%s════════════════════%s\n",
		left,
		middle,
		middle,
		middle,
		middle,
		middle,
		middle,
		right
	);
}

CGU_Time* CGU_formated_time(double time) {
	CGU_Time *cguTime = malloc(sizeof(CGU_Time));
	cguTime->ms = ((time - (long)time) * 1000);
	cguTime->second = (unsigned long) time % 60;
	cguTime->minute = (unsigned long) time / 60 % 60;
	cguTime->hour = (unsigned long) time / 3600 % 3600;

	return cguTime;
}

void CGU_time_free(CGU_Time *time) {
	if (time != NULL) {
		free(time);
	}
}

void CGU_print_vector_range(double *vector, unsigned int start, unsigned int end, char *format, unsigned int reverse) {
	unsigned int i;
	for (i = start; i > end; (reverse ? i-- : i++)) {
		printf(format == NULL ? "%f " : format, vector[i]);
	}
}

void CGU_print_vector(double *vector, unsigned int size, char *format) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		printf(format == NULL ? "%f " : format, vector[i]);
	}
}

void CGU_print_int_vector(unsigned int *vector, unsigned int size, char *format) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		printf(format, vector[i]);
	}
}

void CGU_sub_timespec(struct timespec t1, struct timespec t2, struct timespec *td) {
	enum { NS_PER_SECOND = 1000000000 };
    td->tv_nsec = t2.tv_nsec - t1.tv_nsec;
    td->tv_sec  = t2.tv_sec - t1.tv_sec;
    if (td->tv_sec > 0 && td->tv_nsec < 0)
    {
        td->tv_nsec += NS_PER_SECOND;
        td->tv_sec--;
    }
    else if (td->tv_sec < 0 && td->tv_nsec > 0)
    {
        td->tv_nsec -= NS_PER_SECOND;
        td->tv_sec++;
    }
}