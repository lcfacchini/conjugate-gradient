#ifndef CG_H
#define CG_H

#include "hbmatrix.h"
#include "precond.h"
#include "clenviroment.h"

#include <time.h>

#define CG_MAX_ITERATIONS 10000000

#define CG_EPSILON 0.00001

enum CG_Method {CG, BICG};

typedef struct {
	HB_Matrix *matrix;
	unsigned int numIterations;
	unsigned int convergedIteration;
	double time;
	double *variables;
	double *actualVectorB;
	double *diffVectorB;
	unsigned int printSimplified;
	enum CG_Method method;
	double convergencePercent;
	unsigned int precond;
	enum PC_Preconditioner preconditioner;
	unsigned int cl;
} CG_Result;

typedef struct {
	double *vectorAux;
	double *vectorR;
	double *vectorD;
	double *vectorQ;
	double *vectorS;
	unsigned int numVariables;
} CG_Data;



void CG_conjugate_gradient(HB_Matrix *matrix, double *vectorB, CG_Result *result, unsigned int maxIterations, double epsilon, PC_Matrix *precondMatrix);

void CG_biconjugate_gradient(HB_Matrix *matrix, double *vectorB, CG_Result *result, unsigned int maxIterations, double epsilon, PC_Matrix *precondMatrix);

void CG_vector_subtraction_vector(double *v1, double *v2, double *result, unsigned int size);

double CG_vector_product_vector(double *v1, double *v2, unsigned int size);

void CG_vector_sum_vector(double *v1, double *v2, double *result, unsigned int size);

void CG_vector_attribution(double *v1, double *v2, unsigned int size);

void CG_scalar_product_vector(double scalar, double *vector, double *result, unsigned int size);

void CG_analize_result(HB_Matrix *matrix, CG_Result *result, double *variables, double *vectorB);

void CG_vector_op_scalar_product_vector(double *v1, unsigned int op, double scalar, double *v2, double *vectorResult, unsigned int size);

#endif
