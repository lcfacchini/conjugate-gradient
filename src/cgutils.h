#ifndef CGUTILS_H
#define CGUTILS_H

#include "cg.h"
#include <stdlib.h>
#include <stdio.h>
#include <libgen.h>
#include <math.h>

typedef struct {
	unsigned long hour;
	unsigned long minute;
	unsigned long second;
	unsigned long ms;
} CGU_Time;

void CGU_show_result(CG_Result *result);

void CGU_print_div(char const *left, char const *middle, char const *right);

CGU_Time* CGU_formated_time(double time);

void CGU_time_free(CGU_Time *time);

void CGU_print_vector(double *vector, unsigned int size, char *format);

void CGU_print_vector_range(double *vector, unsigned int start, unsigned int end, char *format, unsigned int reverse);

void CGU_sub_timespec(struct timespec t1, struct timespec t2, struct timespec *td);

#endif
