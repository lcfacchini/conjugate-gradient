#include "hbmatrix.h"
#include "precond.h"

PC_Matrix* PC_diagonal(HB_Matrix *matrix) {
    unsigned int numVariables = matrix->sizePointers - 1;

    DA_Matrix *daMatrix = malloc(sizeof(DA_Matrix));
    daMatrix->values = calloc(numVariables, sizeof(double));
    daMatrix->size = numVariables;

    unsigned int i, j, startRange, endRange;
    double value;
    for (i = 0; i < numVariables; i++) {
        startRange = matrix->pointers[i] - 1;
        endRange = matrix->pointers[i + 1] - 1;

        for (j = startRange; j < endRange; j++) {
            if (matrix->indices[j] - 1 == i) {
                value = matrix->values[j];
                daMatrix->values[i] = value == 0 ? 0 : 1 / value;

                continue;
            }
        }
    }

    PC_Matrix *precondMatrix = malloc(sizeof(PC_Matrix));
    precondMatrix->type = DIAG;
    precondMatrix->daMatrix = daMatrix;
    precondMatrix->preconditioner = DIAGONAL;

    return precondMatrix;
}

PC_Matrix* PC_polynomial(HB_Matrix *matrix) {
    unsigned int numVariables = matrix->sizePointers - 1;
    HB_Matrix *hbMatrix = malloc(sizeof(HB_Matrix));
    *hbMatrix = *matrix;
    hbMatrix->values = malloc(sizeof(double) * hbMatrix->sizeValues);
    *(hbMatrix->values) = *(matrix->values);

    DA_Matrix *diagonal = PC_diagonal(matrix)->daMatrix;

    unsigned int i, j, startRange, endRange, index;
    double value;
    for (i = 0; i < numVariables; i++) {
        startRange = matrix->pointers[i] - 1;
        endRange = matrix->pointers[i + 1] - 1;

        for (j = startRange; j < endRange; j++) {
            index = matrix->indices[j] - 1;
            value = matrix->values[j];
            if (index == i) {
                hbMatrix->values[j] = diagonal->values[index];
            } else {
                hbMatrix->values[j] = diagonal->values[i] * ((diagonal->values[index] * value));
            }
        }
    }

    PC_Matrix *precondMatrix = malloc(sizeof(PC_Matrix));
    precondMatrix->type = HB;
    precondMatrix->hbMatrix = hbMatrix;
    precondMatrix->preconditioner = POLYNOMIAL;

    return precondMatrix;
}