#ifndef PRECOND_H
#define PRECOND_H

enum PC_Matrix_Type {DIAG, HB};

enum PC_Preconditioner {NONE, DIAGONAL, POLYNOMIAL};

typedef struct {
    double *values;
    int size;
} DA_Matrix;

typedef struct {
    union {
        HB_Matrix *hbMatrix;
        DA_Matrix *daMatrix;
    };
    enum PC_Matrix_Type type;
    enum PC_Preconditioner preconditioner;
} PC_Matrix;

PC_Matrix* PC_diagonal(HB_Matrix *matrix);

PC_Matrix* PC_polynomial(HB_Matrix *matrix);

#endif
