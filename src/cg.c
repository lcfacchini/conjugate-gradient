#include "cg.h"
#include "cgutils.h"

void CG_vector_subtraction_vector(double *v1, double *v2, double *result, unsigned int size) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		result[i] = v1[i] - v2[i];
	}
}

double CG_vector_product_vector(double *v1, double *v2, unsigned int size) {
	double sum = 0;
	unsigned int i;
	for (i = 0; i < size; i++) {
		sum += v1[i] * v2[i];
	}

	return sum;
}

void CG_vector_sum_vector(double *v1, double *v2, double *result, unsigned int size) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		result[i] = v1[i] + v2[i];
	}
}

void CG_vector_attribution(double *v1, double *v2, unsigned int size) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		v1[i] = v2[i];
	}
}

void CG_scalar_product_vector(double scalar, double *vector, double *result, unsigned int size) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		result[i] = scalar * vector[i];
	}
}

double vector_sum(double *vector, unsigned int size) {
	double sum = 0.0;
	unsigned int i;
	for (i = 0; i < size; i++) {
		sum += fabs(vector[i]);
	}

	return sum;
}

void CG_precond_matrix_product_vector(PC_Matrix *matrix, double *vector, double *vectorResult, unsigned int size) {
	switch (matrix->type) {
		case DIAG:
			memset(vectorResult, 0, sizeof(double) * size);

			unsigned int i;
			for (i = 0; i < size; i++) {
				vectorResult[i] = matrix->daMatrix->values[i] * vector[i];
			}

			break;
		case HB:
			HB_matrix_product_vector(matrix->hbMatrix, vector, vectorResult, size, 0);

			break;
	}
}

void CG_vector_op_scalar_product_vector(double *v1, unsigned int op, double scalar, double *v2, double *vectorResult, unsigned int size) {
	unsigned int i;
	if (op == '+') {
		for (i = 0; i < size; i++) {
			vectorResult[i] = v1[i] + scalar * v2[i];
		}
	} else if (op == '-') {
		for (i = 0; i < size; i++) {
			vectorResult[i] = v1[i] - scalar * v2[i];
		}
	}
}

void CG_conjugate_gradient(HB_Matrix *matrix,
							double *vectorB,
							CG_Result *result,
							unsigned int maxIterations,
							double epsilon,
							PC_Matrix *precondMatrix)
{
	unsigned int numVariables = matrix->sizePointers - 1, precond = precondMatrix != NULL;

	clock_t startTime = clock();

	double *vectorAux = (double *) calloc(numVariables, sizeof(double));
	double *vectorR = (double *) calloc(numVariables, sizeof(double));
	double *vectorD = (double *) calloc(numVariables, sizeof(double));
	double *vectorQ = (double *) calloc(numVariables, sizeof(double));
	double *vectorS;

	if (precond) {
	 	vectorS = (double *) calloc(numVariables, sizeof(double));
	}

	double newSigma, sigma0, oldSigma, alpha, beta;

	HB_matrix_product_vector(matrix, result->variables, vectorAux, numVariables, 0);

	CG_vector_subtraction_vector(vectorB, vectorAux, vectorR, numVariables);

	if (precond) {
		CG_precond_matrix_product_vector(precondMatrix, vectorR, vectorD, numVariables);
	} else {
		CG_vector_attribution(vectorD, vectorR, numVariables);
	}

	newSigma = CG_vector_product_vector(vectorR, precond ? vectorD : vectorR, numVariables);
	sigma0 = newSigma;

	while (result->numIterations < maxIterations && newSigma > epsilon * epsilon * sigma0) {
		HB_matrix_product_vector(matrix, vectorD, vectorQ, numVariables, 0);

		alpha = newSigma / CG_vector_product_vector(vectorD, vectorQ, numVariables);

		CG_vector_op_scalar_product_vector(result->variables, '+', alpha, vectorD, result->variables, numVariables);

		if (result->numIterations % 50 == 0) {
			HB_matrix_product_vector(matrix, result->variables, vectorAux, numVariables, 0);
			CG_vector_subtraction_vector(vectorB, vectorAux, vectorR, numVariables);
		} else {
			CG_vector_op_scalar_product_vector(vectorR, '-', alpha, vectorQ, vectorR, numVariables);
		}

		if (precond) {
			CG_precond_matrix_product_vector(precondMatrix, vectorR, vectorS, numVariables);
		}

		oldSigma = newSigma;
		newSigma = CG_vector_product_vector(vectorR, precond ? vectorS : vectorR, numVariables);
		beta = newSigma / oldSigma;

		CG_vector_op_scalar_product_vector(precond ? vectorS : vectorR, '+', beta, vectorD, vectorD, numVariables);

		result->numIterations++;
	}

	result->convergedIteration = result->numIterations;
	result->time = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;

	free(vectorAux);
	free(vectorR);
	free(vectorD);
	free(vectorQ);
}

void CG_biconjugate_gradient(HB_Matrix *matrix,
							double *vectorB,
							CG_Result *result,
							unsigned int maxIterations,
							double epsilon,
							PC_Matrix *precondMatrix)
{
	unsigned int numVariables = matrix->sizePointers - 1, precond = precondMatrix != NULL, i = 0;

	clock_t startTime = clock();

	double *vectorAux = (double *) calloc(numVariables, sizeof(double));
	double *vectorR = (double *) calloc(numVariables, sizeof(double));
	double *vectorR2 = (double *) calloc(numVariables, sizeof(double));
	double *vectorP = (double *) calloc(numVariables, sizeof(double));
	double *vectorP2 = (double *) calloc(numVariables, sizeof(double));
	double *vectorV = (double *) calloc(numVariables, sizeof(double));
	double *vectorV2 = (double *) calloc(numVariables, sizeof(double));
	double *vectorS = (double *) calloc(numVariables, sizeof(double));
	double *vectorS2 = (double *) calloc(numVariables, sizeof(double));

	double alpha, beta, rho, rho0;

	HB_matrix_product_vector(matrix, result->variables, vectorAux, numVariables, 0);
	CG_vector_subtraction_vector(vectorB, vectorAux, vectorR, numVariables);

	CG_vector_attribution(vectorR2, vectorR, numVariables);

	rho = rho0 = 1;

	while (result->numIterations < maxIterations) {
		rho0 = rho;
		if (precond) {
			CG_precond_matrix_product_vector(precondMatrix, vectorR, vectorAux, numVariables);
		}
		rho = CG_vector_product_vector(vectorR2, precond ? vectorAux : vectorR, numVariables);
		beta = rho / rho0;

		if (precond) {
			CG_precond_matrix_product_vector(precondMatrix, vectorR, vectorS, numVariables);
			CG_precond_matrix_product_vector(precondMatrix, vectorR2, vectorS2, numVariables);
		}

		for (i = 0; i < numVariables; i++) {
			vectorP[i] = (precond ? vectorS[i] : vectorR[i]) + beta * vectorP[i];
			vectorP2[i] = (precond ? vectorS2[i] : vectorR2[i]) + beta * vectorP2[i];
		}

		HB_matrix_product_vector(matrix, vectorP, vectorV, numVariables, 0);

		alpha = rho / CG_vector_product_vector(vectorP2, vectorV, numVariables);

		CG_vector_op_scalar_product_vector(result->variables, '+', alpha, vectorP, result->variables, numVariables);

		if (CG_vector_product_vector(vectorR, vectorR, numVariables) <= epsilon * epsilon) {
			break;
		}

		HB_matrix_product_vector(matrix, vectorP2, vectorV2, numVariables, 1);
		for (i = 0; i < numVariables; i++) {
			vectorR[i] = vectorR[i] - alpha * vectorV[i];
			vectorR2[i] = vectorR2[i] - alpha * vectorV2[i];
		}

		result->numIterations++;
	}

	result->convergedIteration = result->numIterations;
	result->time = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;

	free(vectorAux);
	free(vectorR);
	free(vectorR2);
	free(vectorP);
	free(vectorP2);
	free(vectorV);
}

void CG_analize_result(HB_Matrix *matrix, CG_Result *result, double *variables, double *vectorB) {
	double *vectorAux = (double *) calloc(matrix->sizePointers - 1, sizeof(double));
	double diffPercent = 0.0, percent;
	unsigned int i;

	HB_matrix_product_vector(matrix, variables, vectorAux, matrix->sizePointers - 1, 0);
	for (i = 0; i < matrix->sizePointers - 1; i++) {
		result->diffVectorB[i] = vectorB[i] - fabs(vectorAux[i]);
		diffPercent += fabs(vectorB[i] - vectorAux[i]) / vectorB[i];
	}
	percent = 1 - (diffPercent / (matrix->sizePointers - 1));

	result->convergencePercent = (percent >= 0 && percent <= 1) ? percent : 0;
	result->actualVectorB = vectorAux;
}