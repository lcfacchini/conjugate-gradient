#include "hbmatrix.h"

void HB_matrix_product_vector(HB_Matrix *matrix,
							  double *vector,
							  double *result,
							  unsigned int size,
							  unsigned int transpose)
{
	unsigned int i, j, startRange, endRange, index;
	double value;

	memset(result, 0, sizeof(double) * size);

	for (i = 0; i < size; i++) { // coluna
		startRange = matrix->pointers[i] - 1;
		endRange = matrix->pointers[i + 1] - 1;

		for (j = startRange; j < endRange; j++) {
			index = matrix->indices[j] - 1; // linha
			value = matrix->values[j];

			result[transpose ? i : index] += vector[transpose ? index : i] * value;
			if (index != i && matrix->type == HB_MATRIX_TYPE_SYMMETRIC) {
				result[transpose ? index : i] += vector[transpose ? i : index] * value;
			}
		}
	}
}

void HB_matrix_free(HB_Matrix *matrix) {
	if (matrix != NULL) {
		if (matrix->pointers != NULL) {
			free(matrix->pointers);
		}

		if (matrix->indices != NULL) {
			free(matrix->indices);
		}

		if (matrix->values != NULL) {
			free(matrix->values);
		}

		free(matrix);
	}
}

double *HB_build_dense_matrix(HB_Matrix *matrix) {
	unsigned int numVariables = matrix->sizePointers - 1, i, j, start, end;

	double *denseMatrix = calloc(numVariables * numVariables, sizeof(double));

	for (i = 0; i < numVariables; i++) {
        start = matrix->pointers[i] - 1;
        end = matrix->pointers[i + 1] - 1;

        if (start < end) {
            for (j = start; j < end; j++) {
				if (matrix->format == HB_MATRIX_CSC) {
                	denseMatrix[((matrix->indices[j] - 1) * numVariables) + i] = matrix->values[j];
				} else if (matrix->format == HB_MATRIX_CSR) {
					denseMatrix[i * numVariables + (matrix->indices[j] - 1)] = matrix->values[j];
				}
            }
        }
    }

	return denseMatrix;
}

void HB_print_matrix(HB_Matrix *matrix) {
	unsigned int numVariables = matrix->sizePointers - 1, i, j;
	double *denseMatrix = HB_build_dense_matrix(matrix);

	printf("\n");
	for (i = 0; i < numVariables; i++) {
        for (j = 0; j < numVariables; j++) {
			printf("%.10f ", denseMatrix[i * numVariables + j]);
        }
        printf("; \n");
    }
	printf("\n");
}

HB_Matrix *HB_csc2csr(HB_Matrix *matrix) {
	HB_Matrix *matrixB = malloc(sizeof(HB_Matrix));
	*matrixB = *matrix;

	matrixB->format = HB_MATRIX_CSR;
	matrixB->pointers = calloc(sizeof(int), matrix->sizePointers);
	matrixB->indices = calloc(sizeof(int), matrix->sizeIndices);
	matrixB->values = calloc(sizeof(double), matrix->sizeIndices);

	unsigned int n, nnz = matrix->sizeValues, n_row = matrix->sizePointers, jj, row;

	for (n = 0; n < nnz; n++){
        matrixB->pointers[matrix->indices[n]-1]++;
    }

	unsigned int col, dest, temp, cumsum, last;
    for(col = 0, cumsum = 1; col < n_row; col++){
        temp  = matrixB->pointers[col];
        matrixB->pointers[col] = cumsum;
        cumsum += temp;
    }

    for(row = 0; row < n_row - 1; row++){
        for(jj = (matrix->pointers[row] - 1); jj < (matrix->pointers[row + 1] - 1); jj++){
            col  = matrix->indices[jj] - 1;
            dest = matrixB->pointers[col] - 1;

            matrixB->indices[dest] = row + 1;
            matrixB->values[dest] = matrix->values[jj];

            matrixB->pointers[col]++;
        }
    }

    for(col = 0, last = 1; col <= n_row - 1; col++){
        temp  = matrixB->pointers[col];
        matrixB->pointers[col] = last;
        last = temp;
    }

	return matrixB;
}