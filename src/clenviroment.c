#include "clenviroment.h"
//#include "cg.h"
#include <unistd.h>
#include <time.h>

CL_Enviroment *CL_start_env() {
    int err = 0, gpu = 1;

    CL_Enviroment *enviroment = malloc(sizeof(CL_Enviroment));
    cl_platform_id platform;

    err |= clGetPlatformIDs(1, &platform, NULL);

    err = clGetDeviceIDs(platform, gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, 1, &(enviroment->deviceId), NULL);
    if (err != CL_SUCCESS) {
        return NULL;
    }

    enviroment->context = clCreateContext(0, 1, &(enviroment->deviceId), NULL, NULL, &err);
    if (err != CL_SUCCESS) {
        return NULL;
    }

    enviroment->queue = clCreateCommandQueueWithProperties(enviroment->context, enviroment->deviceId, NULL, &err);
    if (err != CL_SUCCESS) {
        return NULL;

    }

    char *kernelSource = CL_read_kernel_file("src/cg.cl");
    if (kernelSource == NULL) {
        return NULL;
    }

    enviroment->program = clCreateProgramWithSource(enviroment->context, 1, (const char **) &kernelSource, NULL, &err);
    if (! enviroment->program) {
        printf("Error: Failed to create compute program!\n");

        return NULL;
    }

    err = clBuildProgram(enviroment->program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n");
        clGetProgramBuildInfo(enviroment->program, enviroment->deviceId, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);

        return NULL;
    }

    CL_prepare_kernels(enviroment);

    return enviroment;
}

char *CL_read_kernel_file(char *kernelFilePath) {
	FILE *kernelFile = fopen(kernelFilePath, "rb");
    if (kernelFile == NULL) {
        return NULL;
    }
    long fileSize;

    fseek(kernelFile, 0, SEEK_END);
    fileSize = ftell(kernelFile);
    fseek(kernelFile, 0, SEEK_SET);

    char *content = (char *) malloc(fileSize + 1);
    fread(content, 1, fileSize, kernelFile);
    content[fileSize] = '\0';
    fclose(kernelFile);

    return content;
}

cl_kernel CL_create_kernel(CL_Enviroment *clEnv,  char *kernelName) {
    int err = 0;
    cl_kernel kernel = malloc(sizeof(cl_kernel));

	kernel = clCreateKernel(clEnv->program, kernelName, &err);
    if (!kernel || err != CL_SUCCESS) {
        printf("Error: Failed to create compute kernel!\n");
    }
    return kernel;
}

void CL_prepare_kernels(CL_Enviroment *env) {
    env->kernels = malloc(sizeof(CL_Kernels));
    env->kernels->matrixProductVector = CL_create_kernel(env, "matrix_product_vector");
	env->kernels->diagonalMatrixProductVector = CL_create_kernel(env, "diagonal_matrix_product_vector");
	env->kernels->vectorProductVector = CL_create_kernel(env, "vector_product_vector");
	env->kernels->cgSigma0 = CL_create_kernel(env, "cg_sigma0");
	env->kernels->cgBeta = CL_create_kernel(env, "cg_beta");
	env->kernels->cgAlpha = CL_create_kernel(env, "cg_alpha");
	env->kernels->vectorSubtractionVector = CL_create_kernel(env, "vector_subtraction_vector");
	env->kernels->vectorSumScalarProductVector = CL_create_kernel(env, "vector_sum_scalar_product_vector");
	env->kernels->vectorSubScalarProductVector = CL_create_kernel(env, "vector_sub_scalar_product_vector");
	env->kernels->vectorAttribution = CL_create_kernel(env, "vector_attribution");
	env->kernels->bicgResiduals = CL_create_kernel(env, "bicg_residuals");
	env->kernels->bicgDirections = CL_create_kernel(env, "bicg_directions");
	env->kernels->bicgBeta = CL_create_kernel(env, "bicg_beta");
}

void CLHB_matrix_product_vector(CL_Enviroment *clEnv,
                                cl_kernel kernel,
                                HB_Matrix *matrix,
                                HB_Matrix_CL *matrixCl,
                                double *vector,
                                double *result,
                                unsigned int transpose)
{
    int err = 0;
	unsigned int numVariables = matrix->sizePointers - 1;

    cl_mem clVector = clCreateBuffer(clEnv->context, CL_MEM_READ_ONLY, sizeof(double) * numVariables, NULL, NULL);
    cl_mem clResult = clCreateBuffer(clEnv->context, CL_MEM_WRITE_ONLY, sizeof(double) * numVariables, NULL, NULL);
    cl_mem clTranspose = clCreateBuffer(clEnv->context, CL_MEM_READ_ONLY, sizeof(unsigned int), NULL, NULL);

    err |= clEnqueueWriteBuffer(clEnv->queue, clVector, CL_TRUE, 0, sizeof(double) * numVariables, vector, 0, NULL, NULL);
    err |= clEnqueueWriteBuffer(clEnv->queue, clTranspose, CL_TRUE, 0, sizeof(unsigned int), &transpose, 0, NULL, NULL);

	err |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &(matrixCl->pointersCsr));
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &(matrixCl->indicesCsr));
    err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &(matrixCl->valuesCsr));

    err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &(matrixCl->pointersCsc));
    err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &(matrixCl->indicesCsc));
    err |= clSetKernelArg(kernel, 5, sizeof(cl_mem), &(matrixCl->valuesCsc));
    err |= clSetKernelArg(kernel, 6, sizeof(cl_mem), &(matrixCl->type));

    err |= clSetKernelArg(kernel, 7, sizeof(cl_mem), &clVector);
    err |= clSetKernelArg(kernel, 8, sizeof(cl_mem), &clResult);
    err |= clSetKernelArg(kernel, 9, sizeof(cl_mem), &clTranspose);

	if (err != CL_SUCCESS) {
        printf("Error: Failed to set kernel arguments! %d\n", err);
        exit(1);
    }

	size_t global[] = {numVariables, 1};
    err = clEnqueueNDRangeKernel(clEnv->queue, kernel, 1, NULL, global, NULL, 0, NULL, NULL);
    if (err) {
        printf("Error: Failed to execute kernel!\n");
        exit(1);
    }
    clFinish(clEnv->queue);

    err = clEnqueueReadBuffer(clEnv->queue, clResult, CL_TRUE, 0, sizeof(double) * numVariables, result, 0, NULL, NULL);
	if (err != CL_SUCCESS) {
        printf("Error: Failed to read output array! %d\n", err);
        exit(1);
    }
}

double CLCG_vector_product_vector(CL_Enviroment *clEnv, cl_kernel kernel, double *v1, double *v2, unsigned int size) {
    size_t workGroupSize[] = {size <= MAX_WORK_GROUP_SIZE ? MIN_WORK_GROUP_SIZE : MAX_WORK_GROUP_SIZE, 1};
    size_t global[] = {((size / workGroupSize[0]) * workGroupSize[0]) + workGroupSize[0], 1};
    unsigned int groupQty = global[0] / workGroupSize[0];

	cl_mem clVector1 = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY, sizeof(double) * size, NULL, NULL);
	cl_mem clVector2 = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY, sizeof(double) * size, NULL, NULL);
	cl_mem clResult = clCreateBuffer(clEnv->context,  CL_MEM_WRITE_ONLY, sizeof(double), NULL, NULL);
	cl_mem clSize = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY, sizeof(unsigned int), NULL, NULL);
	cl_mem clGroupResult = clCreateBuffer(clEnv->context,  CL_MEM_READ_WRITE, sizeof(double) * groupQty, NULL, NULL);
	cl_mem clGroupQty = clCreateBuffer(clEnv->context,  CL_MEM_READ_ONLY, sizeof(unsigned int), NULL, NULL);
    if (!clVector1 || !clVector2) {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }

    int err = clEnqueueWriteBuffer(clEnv->queue, clVector1, CL_TRUE, 0, sizeof(double) * size, v1, 0, NULL, NULL);
	err |= clEnqueueWriteBuffer(clEnv->queue, clVector2, CL_TRUE, 0, sizeof(double) * size, v2, 0, NULL, NULL);
	err |= clEnqueueWriteBuffer(clEnv->queue, clSize, CL_TRUE, 0, sizeof(unsigned int), &size, 0, NULL, NULL);
	err |= clEnqueueWriteBuffer(clEnv->queue, clGroupQty, CL_TRUE, 0, sizeof(unsigned int), &groupQty, 0, NULL, NULL);

    if (err != CL_SUCCESS) {
        printf("Error: failed to write!\n");
        exit(1);
    }

    err |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &clVector1);
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &clVector2);
    err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &clGroupResult);
    err |= clSetKernelArg(kernel, 3, sizeof(double) * workGroupSize[0], NULL);
    err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &clSize);

    cl_kernel totalKernel = CL_create_kernel(clEnv, "vector_product_vector_total");
    err |= clSetKernelArg(totalKernel, 0, sizeof(cl_mem), &clGroupResult);
    err |= clSetKernelArg(totalKernel, 1, sizeof(cl_mem), &clResult);
    err |= clSetKernelArg(totalKernel, 2, sizeof(cl_mem), &clGroupQty);

    if (err != CL_SUCCESS) {
        printf("Error: failed to set args!\n");
        exit(1);
    }

    size_t globalTotal[] = {1, 1};
    for (size_t i = 0; i < 1000; i++) {
        err = clEnqueueNDRangeKernel(clEnv->queue, kernel, 1, NULL, global, workGroupSize, 0, NULL, NULL);
        err |= clEnqueueNDRangeKernel(clEnv->queue, totalKernel, 1, NULL, globalTotal, NULL, 0, NULL, NULL);
        if (err) {
            printf("Error: Failed to execute kernel!\n");
            exit(1);
        }
    }
    clFinish(clEnv->queue);

    double result = 0;

    err = clEnqueueReadBuffer(clEnv->queue, clResult, CL_TRUE, 0, sizeof(double), &result, 0, NULL, NULL);
	if (err != CL_SUCCESS) {
        printf("Error: Failed to read output array! %d\n", err);

        exit(1);
    }

    clReleaseMemObject(clVector1);
    clReleaseMemObject(clVector2);
    clReleaseMemObject(clResult);
    clReleaseMemObject(clSize);
    clReleaseMemObject(clGroupResult);

    return result;
}

void CLCG_vector_op_vector(CL_Enviroment *clEnv,
                                cl_kernel kernel,
                                double *v1,
                                double *v2,
                                double *result,
                                unsigned int size)
{
	cl_mem clVector1 = clCreateBuffer(clEnv->context, CL_MEM_READ_ONLY, sizeof(double) * size, NULL, NULL);
	cl_mem clVector2 = clCreateBuffer(clEnv->context, CL_MEM_READ_ONLY, sizeof(double) * size, NULL, NULL);
	cl_mem clResult = clCreateBuffer(clEnv->context, CL_MEM_WRITE_ONLY, sizeof(double) * size, NULL, NULL);
    if (!clVector1 || !clVector2 || !clResult) {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }

    int err = clEnqueueWriteBuffer(clEnv->queue, clVector1, CL_TRUE, 0, sizeof(double) * size, v1, 0, NULL, NULL);
	err |= clEnqueueWriteBuffer(clEnv->queue, clVector2, CL_TRUE, 0, sizeof(double) * size, v2, 0, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Error: failed to write!\n");
        exit(1);
    }

    err |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &clVector1);
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &clVector2);
    err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &clResult);
    if (err != CL_SUCCESS) {
        printf("Error: failed to set args!\n");
        exit(1);
    }

    size_t global[] = {size, 1};

    err = clEnqueueNDRangeKernel(clEnv->queue, kernel, 1, NULL, global, NULL, 0, NULL, NULL);
    if (err) {
        printf("Error: Failed to execute kernel!\n");
        exit(1);
    }

    clFinish(clEnv->queue);


    err = clEnqueueReadBuffer(clEnv->queue, clResult, CL_TRUE, 0, sizeof(double) * size, result, 0, NULL, NULL);
	if (err != CL_SUCCESS) {
        printf("Error: Failed to read output array! %d\n", err);

        exit(1);
    }

    clReleaseMemObject(clVector1);
    clReleaseMemObject(clVector2);
    clReleaseMemObject(clResult);
}