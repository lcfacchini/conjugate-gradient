#include "cgaparser.h"

CGA_Argument *CGA_parse(int const argc, char *argv[]) {
	struct argp_option options[] = {
		{0, 0, 0, 0, "Method options:", 6},
		{"max-iteration", CGA_MAX_ITERATIONS, "VALUE", 0, "Maximum number of iterations"},
		{"epsilon", CGA_EPSILON, "EPSILON", 0, "Minimal error precision"},
		{"cg", CGA_METHOD_CG, 0, 0, "Conjugate Gradient"},
		{"bicg", CGA_METHOD_BICG, 0, 0, "Biconjugate Gradient (Default)"},
		{"precond", CGA_PRECOND, "VALUE", 0, "Preconditioner (D = Diagonal, P = Polynomial)"},
		{"cl", CGA_USE_OPENCL, 0, 0, "Use OpenCL"},
		{0, 0, 0, 0, "Informational options:", -1},
		{"print-variables", CGA_PRINT_VARS, 0, 0, "Print variables vector result (x)"},
		{"print-actual-b", CGA_PRINT_ACTUAL_B, 0, 0, "Print atual b result (A * x)"},
		{"print-diff-b", CGA_PRINT_DIFF_B, "QTY", 0, "Print actual b difference (b - A * x), ordered desc"},
		{"simplified", CGA_PRINT_SIMPLIFIED, 0, 0, "Print simplified output"},
		{0}
	};

	struct argp argp = {options, CGA_parser, "[FILE...]", "Program to calcule conjugate gradient"};

	CGA_Argument *argument = malloc(sizeof(CGA_Argument));
	argument->printValueVariables = 0;
	argument->maxIterations = CG_MAX_ITERATIONS;
	argument->epsilon = CG_EPSILON;
	argument->method = BICG;
	argument->preconditioner = NONE;

	argp_parse(&argp, argc, argv, 0, 0, argument);

	return argument;
}

error_t CGA_parser(int key, char *arg, struct argp_state *state) {
	CGA_Argument *argument = state->input;

	switch (key) {
		case CGA_PRINT_VARS:
			argument->printValueVariables = 1;

			break;

		case CGA_PRINT_ACTUAL_B:
			argument->printActualB = 1;

			break;

		case CGA_EPSILON:
			argument->epsilon = atof(arg);

			if (argument->epsilon <= 0 || argument->epsilon >= 0.2) {
				argp_failure(state, 1, 0, "Epsilon must be > 0 and < 0.2. Ex.: 0.00001");
			}

			break;

		case CGA_MAX_ITERATIONS:
			argument->maxIterations = atoi(arg);

			break;

		case CGA_METHOD_CG:
			argument->method = CG;

			break;

		case CGA_METHOD_BICG:
			argument->method = BICG;

			break;

		case CGA_PRECOND:
			switch (*arg) {
				case CGA_PRECOND_OPT_DIAGONAL:
					argument->preconditioner = DIAGONAL;

					break;

				case CGA_PRECOND_OPT_POLYNOMIAL:
					argument->preconditioner = POLYNOMIAL;

					break;

				default:
					argument->preconditioner = NONE;

					break;
			}

			break;

		case CGA_USE_OPENCL:
			argument->cl = 1;

			break;

		case CGA_PRINT_DIFF_B:
			argument->printfDiffB = atoi(arg);

			break;

		case CGA_PRINT_SIMPLIFIED:
			argument->printfSimplified = 1;

			break;

		case ARGP_KEY_ARG:
			argument->files = &state->argv[state->next - 1];
			state->next = state->argc;
			break;

		case ARGP_KEY_NO_ARGS:
			argp_usage(state);

			break;

		default:

			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

void CGA_argument_free(CGA_Argument *argument) {
	if (argument != NULL) {
		free(argument);
	}
}
