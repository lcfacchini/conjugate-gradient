#include "main.h"

int main(int const argc, char *argv[]) {
	CGA_Argument *argument = CGA_parse(argc, argv);

	unsigned int fileIndex;
	for (fileIndex = 0; argument->files[fileIndex]; fileIndex++) {
		if (run(argument, argument->files[fileIndex])) {
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}

int run(CGA_Argument *argument, char *filepath) {
	HB_Matrix *matrix = HBFR_read(filepath);
	if (matrix == NULL) {
		printf("Can't open file\n");

		return 1;
	}

	PC_Matrix *precondMatrix;
	switch (argument->preconditioner) {
		case DIAGONAL:
			precondMatrix = PC_diagonal(matrix);

			break;

		case POLYNOMIAL:
			precondMatrix = PC_polynomial(matrix);

			break;

		case NONE:
			precondMatrix = NULL;

			break;
	}

	double *vectorB = (double *) malloc((matrix->sizePointers - 1) * sizeof(double));
	unsigned int i;
	for (i = 0; i < matrix->sizePointers - 1; i++) {
		vectorB[i] = 1;
	}

	CG_Result *result = (CG_Result *) malloc(sizeof(CG_Result));
	result->matrix = matrix;
	result->numIterations = 0;
	result->variables = (double *) calloc(matrix->sizePointers - 1, sizeof(double));
	result->diffVectorB = (double *) calloc(matrix->sizePointers - 1, sizeof(double));
	result->printSimplified = argument->printfSimplified;
	result->precond = argument->preconditioner != NONE;
	result->method = argument->method;
	result->preconditioner = argument->preconditioner;
	result->cl = argument->cl;

	CL_Enviroment *env;
	HB_Matrix_CL *matrixCl;
	if (argument->cl) {
		env = CL_start_env();
		HB_Matrix *matrixCsr = HB_csc2csr(matrix);
		matrixCl = malloc(sizeof(HB_Matrix_CL));

		CG_alloc_matrix_cl(env, matrixCl, matrixCsr, matrix);
	}
	struct timespec start, finish, delta;
	clock_gettime(CLOCK_REALTIME, &start);

	switch (argument->method) {
		case CG:
			if (argument->cl) {
				CG_conjugate_gradient_cl(matrix, matrixCl, vectorB, result, argument->maxIterations, argument->epsilon, precondMatrix, env);
			} else {
				CG_conjugate_gradient(matrix, vectorB, result, argument->maxIterations, argument->epsilon, precondMatrix);
			}

			break;

		case BICG:
			if (argument->cl) {
				CG_biconjugate_gradient_cl(matrix, matrixCl, vectorB, result, argument->maxIterations, argument->epsilon, precondMatrix, env);

			} else {
				CG_biconjugate_gradient(matrix, vectorB, result, argument->maxIterations, argument->epsilon, precondMatrix);
			}

			break;

		default:
			printf("Invalid method\n");

			return 1;
	}

	clock_gettime(CLOCK_REALTIME, &finish);
	CGU_sub_timespec(start, finish, &delta);

	result->time = (int)delta.tv_sec + delta.tv_nsec / 1e9;

	CG_analize_result(matrix, result, result->variables, vectorB);

	CGU_show_result(result);

	if (argument->printValueVariables) {
		CGU_print_vector(result->variables, matrix->sizePointers -1, "%.10f \n");
	}

	if (argument->printActualB) {
		CGU_print_vector(result->actualVectorB, matrix->sizePointers -1, "%.10f \n");
	}

	if (argument->printfDiffB > 0) {
		quicksort(result->diffVectorB, 0, matrix->sizePointers-1);
		CGU_print_vector_range(result->diffVectorB, matrix->sizePointers -1, (matrix->sizePointers -1) - argument->printfDiffB, "%.15f \n", 1);
	}

	HB_matrix_free(matrix);

	return 0;
}