#include "utils.h"


int assert_double_vector_equals_epsilon(double *expected, double *actual, int size, double epsilon) {
	int equal, result = 1;
	unsigned int i;
	for (i = 0; i < size; i++) {
		equal = fabs(expected[i] - actual[i]) < epsilon;
		result &= equal;
		if (! equal) {
			printf("\nE: %.20f \n", expected[i]);
			printf("A: %.20f\n", actual[i]);
		}

		if (size <= 100) {
			printf("%c", equal ? '.' : 'F');
		}
	}

	if (size > 100) {
		printf("%c", result ? '.' : 'F');
	}

	return result;
}

int assert_double_vector_equals(double *expected, double *actual, int size) {
	return assert_double_vector_equals_epsilon(expected, actual, size, EPSILON_DOUBLE_COMPARISON);
}

int assert_double_equals_epsilon(double expected, double actual, double epsilon) {
	int equal = fabs(expected - actual) < epsilon;

	if (! equal) {
		printf("\nE: %.20f \n", expected);
		printf("A: %.20f\n", actual);
	} else {
		printf("%c", equal ? '.' : 'F');
	}

	return equal;
}

int assert_double_equals(double expected, double actual) {
	return assert_double_equals_epsilon(expected, actual, EPSILON_DOUBLE_COMPARISON);
}

double *generate_sequential_vector(unsigned int size) {
	double *vector = malloc(sizeof(double) * size);
	unsigned int i;

	for (i = 0; i < size; i++) {
		vector[i] = i;
	}

	return vector;
}