#define HB_MATRIX_TYPE_SYMMETRIC 'S'
#define HB_MATRIX_TYPE_UNSYMMETRIC 'U'

__kernel void matrix_product_vector(
    __global const unsigned int *pointersCsr,
    __global const unsigned int *indicesCsr,
    __global const double *valuesCsr,

    __global const unsigned int *pointersCsc,
    __global const unsigned int *indicesCsc,
    __global const double *valuesCsc,
    __global const char *type,

    __global const double *vector,
    __global double *result,

    __global const unsigned int *transpose
    )
{
    const unsigned int i = get_global_id(0);

    unsigned int j, startRange, endRange, index;
    unsigned int _transpose = *transpose, _type = *type;

	double value, valueResult = 0;
    startRange = pointersCsr[i] - 1;
    endRange = pointersCsr[i + 1] - 1;
    result[i] = 0;
    if (_type == HB_MATRIX_TYPE_SYMMETRIC || _transpose == 0) {
        for (j = startRange; j < endRange; j++) {
            index = indicesCsr[j] - 1;
            value = valuesCsr[j];

            valueResult += vector[index] * value;
        }
        result[i] = valueResult;
    }

    if (_type == HB_MATRIX_TYPE_SYMMETRIC || _transpose == 1) {
        startRange = pointersCsc[i] - 1;
        endRange = pointersCsc[i + 1] - 1;
        valueResult = 0;
        for (j = startRange; j < endRange; j++) {
            index = indicesCsc[j] - 1;
            value = valuesCsc[j];

            if (i != index || (_transpose == 1 && _type == HB_MATRIX_TYPE_UNSYMMETRIC)) {
                valueResult += vector[index] * value;
            }
        }
        result[i] += valueResult;
    }
}

__kernel void vector_product_vector(
    __global const double *vector1,
    __global const double *vector2,
    __global double *groupResult,
    __local double *localResult,
    __global const unsigned int *size
) {
    const unsigned int id = get_global_id(0);
    const unsigned int localId = get_local_id(0);
    const unsigned int localSize = get_local_size(0);
    const unsigned int groupQty = get_num_groups(0);
    const unsigned int groupId = get_group_id(0);

    unsigned int offset, i, mask;
    if (id < *size) {
        localResult[localId] = vector1[id] * vector2[id];
    } else {
        localResult[localId] = 0;
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    for (offset = 1; offset < localSize; offset *= 2) {
        mask = 2 * offset - 1;
        barrier(CLK_LOCAL_MEM_FENCE);
        if ((localId & mask) == 0) {
            localResult[localId] += localResult[localId + offset];
        }
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (localId == 0) {
        groupResult[groupId] = localResult[0];
    }
}

__kernel void cg_alpha(
    __global const double *partialAlpha,
    __global const unsigned int *sizePartialAlpha,
    __global double *alpha,
    __global const double *newSigma,
    __global double *variables,
    __global const double *vectorD,
    __global const unsigned int *convergence
) {
    const unsigned int id = get_global_id(0), _sizePartialAlpha = *sizePartialAlpha;
    unsigned int i;
    double _partialSum = 0, _alpha;
    for (i = 0; i < _sizePartialAlpha; i++) {
        _partialSum += partialAlpha[i];
    }

    _alpha = *newSigma / _partialSum;
    if (id == 0) {
        *alpha = _alpha;
    }

    if (*convergence == 0) {
        variables[id] = variables[id] + _alpha * vectorD[id];
    }
}

__kernel void vector_product_vector_total(
    __global const double *groupResult,
    __global double *result,
    __global unsigned int *size
) {
    const unsigned int id = get_global_id(0);

    if (id == 0) {
        int i = 0;
        double r = 0;
        for (i = 0; i < (*size); i++) {
            r += groupResult[i];
        }
        *result = r;
    }
}

__kernel void cg_sigma0(
    __global const double *groupResult,
    __global double *result,
    __global unsigned int *size,
    __global double *sigma0
) {
    const unsigned int id = get_global_id(0);

    if (id == 0) {
        int i = 0;
        double r = 0;
        for (i = 0; i < (*size); i++) {
            r += groupResult[i];
        }
        *result = r;
        *sigma0 = r;
    }
}

__kernel void cg_beta(
    __global const double *groupResult,
    __global unsigned int *size,
    __global double *newSigma,
    __global double *oldSigma,
    __global double *beta,
    __global unsigned int *numIterations,
    __global const double *epsilon,
    __global const double *sigma0,
    __global unsigned int *convergence
) {
    const unsigned int id = get_global_id(0);

    if (id == 0 && *convergence == 0) {
        int i = 0;
        double sigmaTotal = 0;
        for (i = 0; i < (*size); i++) {
            sigmaTotal += groupResult[i];
        }
        *oldSigma = *newSigma;
        *newSigma = sigmaTotal;
        *beta = *newSigma / *oldSigma;
        if (*newSigma > *epsilon * *epsilon * *sigma0) {
            *numIterations = *numIterations + 1;
        } else {
            *convergence = 1;
        }
    }
}

__kernel void vector_sum_vector(
    __global const double *vector1,
    __global const double *vector2,
    __global double *result
) {
    const unsigned int id = get_global_id(0);

    result[id] = vector1[id] + vector2[id];
}

__kernel void vector_subtraction_vector(
    __global const double *vector1,
    __global const double *vector2,
    __global double *result
) {
    const unsigned int id = get_global_id(0);

    result[id] = vector1[id] - vector2[id];
}

__kernel void vector_sum_scalar_product_vector(
    __global const double *vector1,
    __global const double *vector2,
    __global double *result,
    __global const double *scalar
) {
    const unsigned int id = get_global_id(0);

    result[id] = vector1[id] + (*scalar * vector2[id]);
}

__kernel void vector_sub_scalar_product_vector(
    __global const double *vector1,
    __global const double *vector2,
    __global double *result,
    __global const double *scalar
) {
    const unsigned int id = get_global_id(0);

    result[id] = vector1[id] - (*scalar * vector2[id]);
}

__kernel void vector_attribution(
    __global double *vector1,
    __global const double *vector2
) {
    const unsigned int id = get_global_id(0);

    vector1[id] = vector2[id];
}

__kernel void diagonal_matrix_product_vector(
    __global double *diagonalVector,
    __global double *vector,
    __global double *result
) {
    const unsigned int id = get_global_id(0);

    result[id] = diagonalVector[id] * vector[id];
}

__kernel void bicg_residuals(
    __global double *alpha,
    __global const double *partialAlpha,
    __global const unsigned int *sizePartialAlpha,
    __global double *rho,
    __global const double *partialRes,
    __global double *res,

    __global double *variables,
    __global const double *vectorP,
    __global double *vectorR,
    __global double *vectorR2,
    __global const double *vectorV,
    __global const double *vectorV2,
    __global unsigned int *convergence

) {
    const unsigned int id = get_global_id(0), _sizePartialAlpha = *sizePartialAlpha;
    unsigned int i;
    double _alpha = 0, _res = 0;

    for (i = 0; i < _sizePartialAlpha; i++) {
        _alpha += partialAlpha[i];
        _res += partialRes[i];
    }
    _alpha = *rho / _alpha;
    if (id == 0) {
        *alpha = _alpha;
        *res = _res;
    }

    if (*convergence == 0) {
        variables[id] = variables[id] + _alpha * vectorP[id];
        vectorR[id] = vectorR[id] - _alpha * vectorV[id];
        vectorR2[id] = vectorR2[id] - _alpha * vectorV2[id];
    }
}

__kernel void bicg_beta(
    __global double *beta,
    __global const double *partialRho,
    __global const unsigned int *sizePartialRho,
    __global double *rho,
    __global double *rho0,
    __global double *res,
    __global unsigned int *convergedIteration,
    __global const double *epsilon,
    __global unsigned int *convergence
) {
    const unsigned int id = get_global_id(0), _sizePartialRho = *sizePartialRho;

    if (id == 0) {
        double _rho0 = *rho0, _rho = *rho, _beta = *beta;
        double _rhoSum = 0;
        unsigned int i;

        for (i = 0; i < _sizePartialRho; i++) {
            _rhoSum += partialRho[i];
        }
        _rho0 = _rho;
        _rho = _rhoSum;
        _beta = _rho / _rho0;

        *rho0 = _rho0;
        *rho = _rho;
        *beta = _beta;

        *convergence = 0;
        if (*convergedIteration > 0 && *res <= *epsilon * *epsilon) {
            *convergence = 1;
        } else {
            *convergedIteration = *convergedIteration + 1;
        }
    }
}

__kernel void bicg_directions(
    __global const double *beta,
    __global double *vectorP,
    __global double *vectorP2,
    __global const double *vectorR,
    __global const double *vectorR2,

    __global const double *diagMatrixPrecond,
    __global const unsigned int *precond
) {
    const unsigned int id = get_global_id(0);

    const double _beta = *beta;
    if (*precond == 1) {
        const double valueS = diagMatrixPrecond[id] * vectorR[id];
        const double valueS2 = diagMatrixPrecond[id] * vectorR2[id];

        vectorP[id] = valueS + _beta * vectorP[id];
        vectorP2[id] = valueS2 + _beta * vectorP2[id];
    } else {
        vectorP[id] = vectorR[id] + _beta * vectorP[id];
        vectorP2[id] = vectorR2[id] + _beta * vectorP2[id];
    }
}

__kernel void zero_vector(__global double *vector) {
    const unsigned int i = get_global_id(0);

    vector[i] = 0;
}
