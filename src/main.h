#ifndef CG_MAIN
#define CG_MAIN

#include "cg.h"
#include "cgcl.h"
#include "cgaparser.h"
#include "hbfreader.h"
#include "hbmatrix.h"
#include "cgutils.h"
#include "precond.h"
#include "clenviroment.h"
#include "sort.h"

int main(int const argc, char *argv[]);

int run(CGA_Argument *argument, char *filepath);

#endif
