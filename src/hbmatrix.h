#ifndef HBMATRIX_H
#define HBMATRIX_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <CL/cl.h>

#define HB_MATRIX_TYPE_SYMMETRIC 'S'
#define HB_MATRIX_TYPE_UNSYMMETRIC 'U'

#define HB_MATRIX_CSR 'R'
#define HB_MATRIX_CSC 'C'

typedef struct {
	char *path;
	unsigned int sizePointers;
	unsigned int sizeIndices;
	unsigned int sizeValues;
	unsigned int *pointers;
	unsigned int *indices;
	double *values;
	char type;
	char format;
} HB_Matrix;

typedef struct {
    cl_mem pointersCsr;
    cl_mem indicesCsr;
    cl_mem valuesCsr;
    cl_mem pointersCsc;
    cl_mem indicesCsc;
    cl_mem valuesCsc;
    cl_mem type;
} HB_Matrix_CL;

void HB_matrix_product_vector(HB_Matrix *matrix, double *vector, double *result, unsigned int size, unsigned int transpose);

void HB_matrix_free(HB_Matrix *matrix);

double *HB_build_dense_matrix(HB_Matrix *matrix);

void HB_print_matrix(HB_Matrix *matrix);

HB_Matrix *HB_csc2csr(HB_Matrix *matrix);

#endif
