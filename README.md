# Paralelização do gradiente conjugado

Neste trabalho foi desenvolvida uma versão paralela dos métodos Gradiente Conjugado (CG) e Gradiente Biconjugado (BiCG), de forma a explorar o paralelismo em arquiteturas de GPU, uma vez que essas possuem uma arquitetura altamente paralelizada e com um hardware otimizado para realização de um grande número de operações de ponto flutuante. Para o desenvolvimento optou-se pela utilização do framework OpenCL, pois esse permite o desenvolvimento da aplicações sem a necessidade do uso de linguagens e ferramentas específicas de fabricante.

## Compilação e execução

Compilação:

```bash
make
```

Obtenção de matrizes:

```bash
make download-hb-files
```

```bash
./bin/conjugate-gradient hb-files/bcsstk01.rsa
```

Para obter mais informações de uso, utilize o comando:

```bash
./bin/conjugate-gradient --usage
```

Para descrever as opções, utilize o comando:

```bash
./bin/conjugate-gradient --help
```

### Matrizes

As matrizes estão armazenadas no repositório [Matrix Market](https://math.nist.gov/MatrixMarket/data/Harwell-Boeing/) em formato de texto [Harwell-Boeing Exchange](https://math.nist.gov/MatrixMarket/formats.html#hb).